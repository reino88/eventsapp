﻿<%@ Page Title="Pealeht" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="EventsApp.Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <%--Main introductory text//--%>

    <div class="row mx-0 mt-3 mainIntroductoryText">
        <div class="col col-md-6 bg-blue">
            <p class="text-align-center text-white lead">
                Sed nec vestibulub, <b>tincidunt orci</b> et, sagittis ex. 
					Vestibulum rutrum <b>neque suscipit</b> ante mattis maximus non sapien <b>viverra,
					lobortis lorem non</b>
                accumasn metus.
            </p>
        </div>

        <%--Main introductory image//--%>
        <div class="col-12 col-md px-0 d-inline-block">
            <img id="pilt" class="img-fluid card-img" src="Content/Images/pilt.jpg" alt="..." />
        </div>
    </div>
      
    <%--Message for client//--%>
    <asp:Label ID="LblClientMessage"
        runat="server"
        CssClass="clientMessage">
       
    </asp:Label>

    <div class="row pt-3 mx-0 pb-5">

        <%--Events--%>
        <%--Upcoming events--%>
        <div class="col-12 col-lg-6 bg-white px-0">

            <%--Gridview header//--%>
            <p class="text-white text-center bg-blue py-2 lead">Tulevased üritused</p>

            <asp:GridView ID="GvUpcomingEvents"
                DataKeyNames="ID"
                OnRowDeleting="GvUpcomingEvents_RowDeleting"
                AutoGenerateColumns="False"
                PageIndex="1"
                ClientIDMode="Static"
                runat="server"
                GridLines="None"
                Width="100%"
                RowStyle-CssClass="item-hover"
                ShowFooter="false"
                ShowHeader="false">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>

                            <div class="rowEvents row text-center text-md-left ml-md-3 ml-lg-0 mr-md-0 pb-2">

                                <div class="col col-md-6 col-lg mr-md-2 break-word">
                                    <asp:Label ID="RowNumer"
                                        CssClass="rowNumber"
                                        runat="server">
                                        <%# Eval("RowNumber") %></asp:Label>.

                                    <%--Event name//--%>
                                    <asp:Label ID="lblEventName"
                                        CssClass="eventName"
                                        runat="server">
                                            <%# Server.HtmlEncode(Eval("Name").ToString()) %></asp:Label>
                                </div>

                                <%--Event date & time//--%>
                                <asp:Label ID="Date"
                                    CssClass="eventDate col-12 col-md-3 col-lg-3 p-lg-0"
                                    runat="server">
                                    <%# Eval("Date") %></asp:Label>

                                <%--Upcoming event actions--%>
                                <div class="eventAction col-12 col-md-2 col-lg-3 mr-lg-3 mr-xl-0 p-md-0">

                                    <%--Move to participants page//--%>
                                    <asp:Button ID="BtnMoveToParticipants"
                                        OnClick="BtnMoveToParticipants_Click"
                                        CommandArgument='<%# Eval("ID") %>' 
                                        Text="Osavõtjad"
                                        runat="server"
                                        BorderStyle="none"
                                        Width="90"
                                        ForeColor="#6c757d"
                                        Font-Bold="true"
                                        CssClass="btnMoveToParticipants btn-link" />

                                    <%--Delete upcoming event//--%>
                                    <asp:ImageButton ID="BtnDeleteUpcomingEvent"
                                        CausesValidation="false"
                                        CommandName="Delete"
                                        ToolTip="Kustuta üritus"
                                        runat="server"
                                        ImageUrl="Content/Images/remove.svg"
                                        ForeColor="#6c757d"
                                        Font-Bold="true"
                                        Height="15px"
                                        CssClass="p-0 btnDeleteEvent"></asp:ImageButton>
                                </div>
                                <%--/.Upcoming event actions--%>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

            <%--Move to event addings//--%>
            <div class="col-12 text-center text-md-left pb-3 pl-md-4 pl-lg-3 pl-0">
                <asp:Button ID="BtnMoveToAddUserEvent"
                    OnClick="BtnMoveToAddUserEvent_Click"
                    runat="server"
                    Text="Lisa üritus"
                    ForeColor="#6c757d"
                    Font-Bold="true"
                    BorderStyle="None"
                    CssClass="btn-link pl-md-2 pl-lg-0"></asp:Button>
            </div>

            <%--Upcoming events pager--%>
            <asp:DataList ID="DlPager1"
                OnItemCommand="DlPager1_ItemCommand"
                RepeatDirection="Horizontal"
                runat="server"
                CssClass="pager mx-auto"
                ShowFooter="false">
                <ItemTemplate>

                    <%--Pager buttons//--%>
                    <asp:Button ID="BtnPagerPageNumber"
                        CommandArgument='<%#Eval("Value") %>'
                        CommandName="PageNo"
                        runat="server"
                        Text='<%#Eval("Text") %>'
                        BorderStyle="None"
                        CssClass="paginate_button btn-link"></asp:Button>
                </ItemTemplate>
            </asp:DataList>
            <%--/.Upcoming events pager--%>
        </div>
        <%--/.Upcoming events--%>

        <!--Past events-->
        <div class="col-12 col-lg bg-white mr-3 mt-3 mt-lg-0 ml-lg-3 mr-lg-0 px-0 overfow-x-auto">

            <!--Gridview header//-->
            <p class="bg-blue text-white text-center pt-2 pb-2 lead">Toimunud üritused</p>

            <asp:GridView ID="GvPastEvents"
                DataKeyNames="ID"
                AutoGenerateColumns="False"
                PageIndex="1"
                ClientIDMode="Static"
                runat="server"
                RowStyle-CssClass="item-hover"
                FooterStyle-CssClass="pb-3"
                GridLines="None"
                Width="100%"
                ShowFooter="false"
                ShowHeader="false">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <div class="dr row text-center text-md-left ml-md-3 ml-lg-0 pb-2">

                                <%--Past event row number & Name//--%>
                                <div class="col col-md-6 col-lg col-xl-6 pr-lg-0 break-word">
                                    <asp:Label ID="RowNumber"
                                        CssClass="col col-md-6 col-lg col-xl-6 pr-lg-0 break-word px-0"
                                        runat="server">
                                        <%# Eval("RowNumber") %></asp:Label>.

                                    <asp:Label ID="Name"
                                        CssClass="eventName"
                                        runat="server">
                                        <%# Server.HtmlEncode(Eval("Name").ToString()) %></asp:Label>
                                </div>

                                <%--Past event date & time//--%>
                                <asp:Label ID="Date"
                                    CssClass="eventDate col-12 col-md-3 col-lg-3 p-lg-0"
                                    runat="server">
                                     <%# Eval("Date") %>
                                </asp:Label>

                                <div class="col-12 col-md col-lg-3 col-xl p-lg-0 pb-2 pl-xl-3">

                                    <%--Move to participants//--%>
                                    <asp:Button ID="BtnPastEvent"
                                        OnClick="BtnMoveToParticipants_Click"
                                        Text="Osavõtjad"
                                        runat="server"
                                        BorderStyle="none"
                                        Width="90"
                                        ForeColor="#6c757d"
                                        Font-Bold="true"
                                        CssClass="btnMoveToParticipants btn-link" />
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

            <%--Past events pager--%>
            <asp:DataList ID="DlPager2"
                ClientIDMode="Static"
                OnItemCommand="DlPager2_ItemCommand"
                RepeatDirection="Horizontal"
                runat="server"
                CssClass="mx-auto pager"
                ShowFooter="false">

                <ItemTemplate>

                    <%--Past events pager buttons//--%>
                    <asp:Button ID="BtnPagerPageNumber"
                        CommandArgument='<%#Eval("Value") %>'
                        CommandName="PageNo"
                        runat="server"
                        Text='<%#Eval("Text") %>'
                        BorderStyle="None"
                        CssClass="paginate_button btn-link"></asp:Button>
                </ItemTemplate>
            </asp:DataList>
            <%--/.Past events pager--%>
        </div>
        <%--/.Past events--%>
    </div>
    <%--/.Events--%>
</asp:Content>
