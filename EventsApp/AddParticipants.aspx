﻿<%@ Page Title="Osalejad" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddParticipants.aspx.cs" Inherits="EventsApp.AddParticipants" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="Content/RadioButtons.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <%--Title and image--%>
    <div class="bg-white pb-5">

        <div class="row text-light mx-0 my-3">
            <div class="col col-sm-4 col-md-3 col-lg-2 bg-blue">
                <p class="text-align-center h4 pt-1 pb-1">Osavõtjad</p>
            </div>
            <div class="col-12 col-sm px-0">
                <img src="Content/Images/libled.jpg" class="img-full img-full-sm img-full-md img-full-lg img-full-xl" />
            </div>
        </div>
        <%--/.Title and image--%>

        <%--Main body--%>

        <div class="container offset-md-1 offset-lg-3 px-2 px-sm-3">

            <%--Message for client//--%>
            <asp:Label ID="LblClientMessage"
                ForeColor="#005aa1"
                runat="server"
                CssClass="mb-5">
            </asp:Label>

            <%--Event and participants output--%>
            <div class="container px-0">

                <%--Heading//--%>
                <label role="heading" class="h5 fg-blue">Osavõtjad</label>

                <%--Event--%>
                <asp:DataList ID="DlEvent"
                    runat="server"
                    CssClass="w-100">

                    <ItemTemplate>

                        <%--Event name//--%>
                        <div class="row">
                            <div class="col-6 col-sm-3 col-lg-2">
                                Ürituse nimi:
                            </div>
                            <asp:Label ID="Name"
                                CssClass="col text-muted pb-1 pl-sm-2"
                                runat="server">
                                <%# Server.HtmlEncode(Eval("Name").ToString())%></asp:Label>
                        </div>

                        <%--Event date time//--%>
                        <div class="row">
                            <div class="col-6 col-sm-3 col-lg-2">
                                Toimumisaeg:
                            </div>

                            <asp:Label ID="Date"
                                CssClass="col text-muted pb-1 pl-sm-2"
                                runat="server">
                            <%# Eval("DateTime")%></asp:Label>
                        </div>

                        <%--Event Location//--%>
                        <div class="row">
                            <div class="col-6 col-sm-3 col-lg-2">
                                Koht:
                            </div>
                            <asp:Label ID="Location"
                                CssClass="col text-muted pb-1 pl-sm-2"
                                runat="server">
                           <%# Server.HtmlEncode(Eval("Location").ToString())%></asp:Label>
                        </div>

                        <div class="row pb-2">
                            <div class="col">
                                Osavõtjad:
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:DataList>
                <%--/.Event--%>

                <%--Participants--%>
                <div class="overfow-x-auto mr-lg-2">

                    <asp:GridView ID="GvParticipants" 
                        DataKeyNames="IdParticipant,ParticipantType"
                        OnRowDeleting="GvParticipants_RowDeleting"
                        AutoGenerateColumns="false"
                        PageIndex="1"
                        ClientIDMode="Static"
                        GridLines="None"
                        runat="server"
                        RowStyle-CssClass="row text-left text-muted item-hover pb-1"
                        ShowHeader="false"
                        CssClass="col col-sm-9 col-md-8 col-lg-7 offset-sm-3 offset-lg-2">

                        <EmptyDataTemplate>Andmed puuduvad</EmptyDataTemplate>

                        <Columns>
                            <asp:TemplateField ItemStyle-CssClass="w-100">
                                <ItemTemplate>

                                    <%--Participant name//--%>
                                    <div id="participant" class="row pl-3 ">
                                        <asp:Label ID="Name" 
                                            CssClass="participantName col-12 col-sm col-md-4 col-lg-5 mr-md-3 break-word-1"
                                            runat="server">
                                            <%# Server.HtmlEncode(Eval("Name").ToString())%>
                                        </asp:Label>

                                        <%--Participant ID//--%>
                                        <asp:Label ID="Label1" 
                                            CssClass="participantID col-12 col-sm pr-md-0 ml-md-3 break-word-1"
                                            runat="server">
                                            <%# Eval("ID") %>
                                        </asp:Label>

                                        <%--Button to participant details//--%>
                                        <div class="col mr-3 row ml-1">
                                            <asp:Button ID="BtnParticipantDetails"
                                                OnClick="BtnParticipantDetails_Click"
                                                CommandArgument='<%# Eval("ParticipantType")%>'
                                                CausesValidation="false"
                                                Text="Vaata"
                                                runat="server"
                                                ForeColor="#6c757d"
                                                Font-Bold="true"
                                                BorderStyle="None"
                                                CssClass="BtnParticipantDetails btn bg-none mr-2 p-0 align-self-start"></asp:Button>

                                            <%--Delete participant--%>
                                            <label class="accordion-1 m-0 p-0">
                                                <div class="accordion-1__div">
                                                    <span class="cursor fg-#6c757d font-weight-bold p-0">Kustuta</span>
                                                    <span class="ml-2 row">
                                                        <input type="checkbox" class="accordion-1__input" />
                                                        <span class="accordion-1__span">
                                                            <asp:Button ID="BtnDeleteParticipant" 
                                                                ClientIDMode="Static"
                                                                OnClientClick="return gvParticipantsDeleteParticipant();"
                                                                CausesValidation="false"
                                                                CommandName="Delete"
                                                                data-value='<%# Eval("IdParticipant") %>'
                                                                runat="server"
                                                                Text="Jah"
                                                                BorderStyle="None"
                                                                CssClass="BtnDeleteParticipant btn-link fg-blue p-0 mr-2"></asp:Button>
                                                        </span>
                                                        <span class="btn-link accordion-1__span">Ei</span>
                                                    </span>
                                                </div>
                                            </label>
                                            <%--/.Delete participant--%>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <%--Participants--%>
            </div>
            <%--/.Event and participants output--%>

            <asp:DataList ID="DlPager1"
                OnItemCommand="DlPager1_ItemCommand"
                ClientIDMode="Static"
                RepeatDirection="Horizontal"
                runat="server"
                CssClass="pager mx-auto" 
                ShowFooter="false">
                <ItemTemplate>

                    <%--Pager buttons//--%>
                    <asp:Button ID="BtnPagerPageNumber" 
                        OnClientClick="return gvPager(bindGvParticipants)"
                        data-value='<%#Eval("Value") %>'
                        CommandArgument='<%#Eval("Value") %>'
                        CommandName="PageNo"
                        ClientIDMode="Static"
                        runat="server"
                        Text='<%#Eval("Text") %>'
                        BorderStyle="None"
                        CssClass="btnPagerPageNumber btn-link px-1"></asp:Button>
                </ItemTemplate>
            </asp:DataList>

            <%--Select participant type--%>
            <div id="RblParticipantType"
                class="col-10 offset-sm-3 offset-lg-2 pl-0"
                runat="server">
                <div class="row">

                    <%--Person--%>
                    <div class="ml-3">
                        <asp:Button ID="BtnRadioTogglerA"
                            OnClick="RadioTogglerA_Click"
                            OnClientClick="javascript: ToggleParticipantForm('RblPerson', 'PnlInsertPerson', 'PnlInsertCompany');"
                            UseSubmitBehavior="true"
                            ClientIDMode="Static"
                            CausesValidation="false"
                            runat="server"
                            CssClass="radio-button-checker" />

                        <input id="RblPerson"
                            type="radio"
                            name="participant"
                            value="person"
                            clientidmode="Static"
                            runat="server" />
                        <label for="RblPerson">Eraisik</label>

                    </div>
                    <%--/.Person--%>

                    <%--Company--%>
                    <div class="ml-3 ml-sm-5">
                        <asp:Button ID="BtnRadioTogglerB"
                            OnClick="RadioTogglerB_Click"
                            OnClientClick="return ToggleParticipantForm('RblCompany', 'PnlInsertCompany', 'PnlInsertPerson');"
                            CausesValidation="false"
                            UseSubmitBehavior="true"
                            ClientIDMode="Static"
                            runat="server"
                            CssClass="radio-button-checker" />

                        <input id="RblCompany"
                            type="radio"
                            name="participant"
                            value="company"
                            clientidmode="Static"
                            runat="server" />
                        <label for="RblCompany">Ettevõte</label>
                    </div>
                    <%--/.Company--%>
                </div>
            </div>
            <%--/.Selecting participant type--%>


            <%--Person--%>
            <asp:Panel ID="PnlInsertPerson"
                CssClass="container text-left"
                OnPreRender="PnlInsertPerson_PreRender"
                ClientIDMode="Static"
                runat="server">

                <%--Person first name--%>
                <div class="form-group row">
                    
                    <label for="TxtPersonFirstName" class="form-label col-sm-3 col-lg-2 pl-0">Eesnimi:</label>
                    
                    <div class="col-12 col-sm-8 col-md-5 col-lg-4 pl-md-0 pr-xl-5 pl-0">
                        <asp:TextBox ID="TxtPersonFirstName"
                            MaxLength="40"
                            runat="server"
                            CssClass="textBoxParticipant form-control form-control-sm">

                        </asp:TextBox>
                        <asp:RequiredFieldValidator ID="RfvTxtPersonFirstName"
                            ControlToValidate="TxtPersonFirstName"
                            ValidationGroup="person"
                            runat="server"
                            Display="Dynamic"
                            Text="Palun sisestage eesnimi"
                            ForeColor="Red"
                            CssClass="participantValidator">

                        </asp:RequiredFieldValidator>
                    </div>
                </div>
                <%--/.Person first name--%>

                <%--Person Surname--%>
                <div class="form-group row">
                    <label for="TxtPersonSurname" class="form-label col-sm-3 col-lg-2 pl-0">Perenimi:</label>
                    <div class="col-12 col-sm-8 col-md-5 col-lg-4 pl-md-0 pr-xl-5 pl-0">

                        <asp:TextBox ID="TxtPersonSurname"
                            MaxLength="40"
                            runat="server"
                            CssClass="textBoxParticipant form-control form-control-sm">

                        </asp:TextBox>
                        <asp:RequiredFieldValidator
                            ID="RfvTxtPersonSurname"
                            ControlToValidate="TxtPersonSurname"
                            ValidationGroup="person"
                            runat="server"
                            Display="Dynamic"
                            Text="Palun sisestage perekonnanimi"
                            ForeColor="Red"
                            CssClass="textBox participantValidator">

                        </asp:RequiredFieldValidator>
                    </div>
                </div>
                <%--/.Person surname--%>

                <%--Person ID--%>
                <div class="form-group row">
                    <label for="TxtPersonPersonalId" class="form-label col-sm-3 col-lg-2 pl-0">Isikukood:</label>
                    <div class="col-12 col-sm-8 col-md-5 col-lg-4 pl-md-0 pr-xl-5 pl-0">

                        <asp:TextBox ID="TxtPersonPersonalId"
                            AutoCompleteType="Disabled"
                            ValidationGroup="person"
                            MaxLength="11"
                            runat="server"
                            CssClass="textBoxParticipant form-control form-control-sm">

                        </asp:TextBox>
                        <asp:RequiredFieldValidator
                            ID="RfvTxtPersonPersonalId"
                            ControlToValidate="TxtPersonPersonalId"
                            ValidationGroup="person"
                            ErrorMessage="RfvTxtPersonPersonalId"
                            Text="Palun sisestage isikukood"
                            runat="server"
                            ForeColor="Red"
                            Display="Dynamic"
                            CssClass="textBox participantValidator">

                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator
                            ID="RevTxtPersonPersonalId"
                            ControlToValidate="TxtPersonPersonalId"
                            ValidationExpression="^([1-9][0-9]{10,10})$"
                            ValidationGroup="person"
                            ErrorMessage="RevTxtPersonPersonalId"
                            runat="server"
                            Text="Palun sisestage korrektne isikukood"
                            Display="Dynamic"
                            ForeColor="Red"
                            CssClass="textBox participantValidator">

                        </asp:RegularExpressionValidator>
                    </div>
                </div>
                <%--/.Person ID--%>

                <%--Person payment--%>
                <div class="form-group row">
                    <label for="DdlPersonPayment" class="form-label col-sm-3 col-lg-2 pl-0">Maksmisviis:</label>
                    <div class="col-12 col-sm-8 col-md-5 col-lg-4 pl-md-0 pr-xl-5 pl-0">

                        <asp:DropDownList ID="DdlPersonPayment"
                            runat="server"
                            CssClass="form-control form-control-sm">

                            <asp:ListItem Value="Ülekanne">Ülekanne</asp:ListItem>
                            <asp:ListItem Value="Sularaha">Sularaha</asp:ListItem>

                        </asp:DropDownList>
                    </div>
                </div>
                <%--/.Person payment--%>

                <%--Person additional info--%>
                <div class="form-group row">
                    <label for="TxtPersonAdditionalInfo" class="form-label col-sm-3 col-lg-2 pl-0">Lisainfo:</label>
                    <div class="col-12 col-sm-8 col-md-5 col-lg-4 pl-md-0 pr-xl-5 pl-0">
                        <asp:TextBox ID="TxtPersonAdditionalInfo"
                            MaxLength="1000"
                            TextMode="MultiLine"
                            runat="server"
                            CssClass="textBoxParticipant form-control form-control-sm">

                        </asp:TextBox>
                    </div>
                </div>
            </asp:Panel>
            <%--/.Person additional info--%>
            <%--/.Person--%>

            <%--Company--%>
            <%--Company name--%>
            <asp:Panel ID="PnlInsertCompany"
                class="container text-left"
                OnPreRender="PnlInsertCompany_PreRender"
                ClientIDMode="Static"
                runat="server">

                <div class="form-group row">
                    <label for="TxtCompanyName" class="form-label col-sm-3 col-lg-2 pl-0">Nimi:</label>
                    <div class="col-12 col-sm-8 col-md-5 col-lg-4 pl-md-0 pr-xl-5 pl-0">
                        <asp:TextBox ID="TxtCompanyName"
                            MaxLength="40"
                            runat="server"
                            CssClass="textBoxParticipant form-control form-control-sm">

                        </asp:TextBox>
                        <asp:RequiredFieldValidator
                            ID="RfvTxtCompanyName"
                            ControlToValidate="TxtCompanyName"
                            ValidationGroup="company"
                            ErrorMessage="RfvTxtCompanyName"
                            runat="server"
                            Display="Dynamic"
                            Text="Palun sisestage üksuse nimi"
                            ForeColor="Red"
                            CssClass="participantValidator">

                        </asp:RequiredFieldValidator>
                    </div>
                </div>
                <%--/.Company name--%>

                <%--Company registry number--%>
                <div class="form-group row">
                    <label for="TxtCompanyRegistryNumber" class="form-label col-sm-3 col-lg-2 pl-0">Registrinumber:</label>
                    <div class="col-12 col-sm-8 col-md-5 col-lg-4 pl-md-0 pr-xl-5 pl-0">
                        <asp:TextBox ID="TxtCompanyRegistryNumber"
                            MaxLength="40"
                            runat="server"
                            CssClass="textBoxParticipant form-control form-control-sm">

                        </asp:TextBox>
                        <asp:RequiredFieldValidator
                            ID="RfvTxtCompanyRegistryNumber"
                            ControlToValidate="TxtCompanyRegistryNumber"
                            ValidationGroup="company"
                            ErrorMessage="RfvTxtCompanyRegistryNumber"
                            runat="server"
                            Display="Dynamic"
                            Text="Palun sisestage registrinumber"
                            ForeColor="Red"
                            CssClass="participantValidator">

                        </asp:RequiredFieldValidator>
                    </div>
                </div>
                <%--/.Company registry number--%>

                <%--Company participants--%>
                <div class="form-group row">
                    <label for="TxtCompanyParticipants" class="form-label col-sm-3 col-lg-2 pl-0">Osalejate arv:</label>
                    <div class="col-12 col-sm-8 col-md-5 col-lg-4 pl-md-0 pr-xl-5 pl-0">
                        <asp:TextBox ID="TxtCompanyParticipants"
                            MaxLength="5"
                            runat="server"
                            CssClass="textBoxParticipant form-control form-control-sm">

                        </asp:TextBox>
                        <asp:RequiredFieldValidator
                            ID="RfvTxtCompanyParticipants"
                            ControlToValidate="TxtCompanyParticipants"
                            ValidationGroup="company"
                            ErrorMessage="RfvTxtCompanyParticipants"
                            runat="server"
                            Display="Dynamic"
                            Text="Palun sisestage osalejate arv"
                            ForeColor="Red"
                            CssClass="participantValidator">

                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator
                            ID="RevTxtCompanyParticipants"
                            ControlToValidate="TxtCompanyParticipants"
                            ValidationExpression="^([1-9][0-9]{0,4})$"
                            ValidationGroup="company"
                            ErrorMessage="RevTxtPersonPersonalId"
                            runat="server"
                            Text="Palun sisestage korrektne arv"
                            Display="Dynamic"
                            ForeColor="Red"
                            CssClass="participantValidator">

                        </asp:RegularExpressionValidator>
                    </div>
                </div>

                <%--/.Company participants--%>

                <%--Company payment--%>
                <div class="form-group row">
                    <label for="DdlCompanyPayment" class="form-label col-sm-3 col-lg-2 pl-0">Maksmisviis:</label>
                    <div class="col-12 col-sm-8 col-md-5 col-lg-4 pl-md-0 pr-xl-5 pl-0">
                        <asp:DropDownList ID="DdlCompanyPayment"
                            runat="server"
                            CssClass="form-control form-control-sm">

                            <asp:ListItem Value="Ülekanne">Ülekanne</asp:ListItem>
                            <asp:ListItem Value="Sularaha">Sularaha</asp:ListItem>

                        </asp:DropDownList>
                    </div>
                </div>
                <%--/.Company payment--%>

                <%--Company additional info--%>
                <div class="form-group row">
                    <label for="TxtCompanyAdditionalInfo"
                        class="form-label col-sm-3 col-lg-2 pl-0">
                        Lisainfo:
                    </label>

                    <div class="col-11 col-sm-8 col-md-5 col-lg-4 pl-md-0 pr-xl-5 pl-0">
                        <asp:TextBox ID="TxtCompanyAdditionalInfo"
                            TextMode="MultiLine"
                            MaxLength="1500"
                            runat="server"
                            CssClass="textBoxParticipant form-control form-control-sm">

                        </asp:TextBox>
                    </div>
                </div>
            </asp:Panel>
            <%--/.Company additional info--%>
            <%--/.Company--%>

            <%--Back to home button//--%>
            <asp:Button ID="BtnBackToHome"
                OnClick="BtnBackToHome_Click"
                CausesValidation="false"
                Text="Tagasi"
                runat="server"
                BorderStyle="None"
                CssClass="btn px-3 mr-2 mt-3" />

            <%--Save button//--%>
            <asp:Button ID="BtnInsertParticipant"
                OnClick="BtnInsertParticipant_Click"
                OnClientClick="return particiPantsInsert('#GvParticipants');"
                CausesValidation="true"
                ValidationGroup="person, company"
                UseSubmitBehavior="true"
                Text="Salvesta"
                runat="server"
                BorderStyle="None"
                CssClass="btn bg-blue text-light px-2 mt-3" />
        </div>
    </div>

    <%--/.Main body--%>
</asp:Content>
