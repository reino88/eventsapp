﻿using EventsApp.JQueryDatatable;
using EventsApp.Participants;
using System.Configuration;
using System.Web.Http;

namespace EventsApp.ApiControllers
{
    /// <summary>
    /// Api controller for participants in event
    /// </summary>
    public class ParticipantsController : ApiController
    {
        private static readonly string _connectionString =
           ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;

        /// <summary>
        /// HttpPost Get all participants from in event
        /// </summary>
        /// <param name="request">Datatable request parameters</param>
        /// <param name="eventId">Event Id where to find participants</param>
        /// <returns>IHttpActionResult OK(DataTableResponse response)</returns>
        [HttpPost]
        [Route("api/GetAllParticipantsInEvent/{eventId}")]
        public IHttpActionResult GetAllParticipantsInEvent(DatatableRequest request, long eventId)
        {
            int recordsTotal = 0;
            DataTableResponse response = new DataTableResponse()
            {
                draw = request.Draw,
                data = ParticipantsAccessLayer.GetAllParticipantsInEvent(eventId, request.Start + 1, out recordsTotal, request.Length).ToArray(),
                recordsTotal = recordsTotal,
                recordsFiltered = recordsTotal,
                error = "",
            };

            return Ok(response);
        }
    }
}