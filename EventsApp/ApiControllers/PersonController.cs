﻿using EventsApp.Participants;
using EventsApp.Participants.PersonInfo;
using Newtonsoft.Json.Linq;
using System;
using System.Web.Http;

namespace EventsApp.ApiControllers
{
    /// <summary>
    /// Api controller for person
    /// </summary>
    public class PersonController : ApiController
    {
        /// <summary>
        /// HttpPost Insert person to event
        /// </summary>
        /// <param name="newParticipant">New participant object to insert</param>
        /// <returns>Returns true if person already exists in event else false</returns>
        [HttpPost]
        [Route("api/InsertPerson")]
        public IHttpActionResult InsertPerson([FromBody]JObject newParticipant)
        {
            long eventId = Convert.ToInt64(newParticipant.SelectToken("newParticipant.eventId"));
            Person person = newParticipant.SelectToken("newParticipant.participant").ToObject<Person>();
            bool isInEvent = false;

            PersonAccessLayer.InsertPerson(eventId, person, out isInEvent);
            return Ok(isInEvent);

        }

        /// <summary>
        /// HttpPut Update person in event
        /// </summary>
        /// <param name="personID">Person id to update</param>
        /// <param name="person">New data for person to update</param>
        /// <returns>returns true if person updated else false</returns>
        [HttpPut]
        [Route("api/UpdatePerson/{PersonID}")]
        public IHttpActionResult UpdatePerson(long personID, [FromBody]Person person)
        {
            PersonAccessLayer.UpdatePerson(personID, person, out bool isUpdated);
            return Ok(isUpdated);
        }

        /// <summary>
        /// HttpPost Delete person from event controller
        /// </summary>
        /// <param name="data">Delete parameters</param>
        /// <returns>Returns true if deleted, esle false</returns>
        [HttpPost]
        [Route("api/DeletePerson")]
        public IHttpActionResult DeletePerson([FromBody]JObject data)
        {
            long eventId = Convert.ToInt64(data["eventId"]);
            long participantId = Convert.ToInt64(data["participantId"]);

            PersonAccessLayer.DeletePersonFromEvent(eventId, participantId, out bool isDeleted);
            return Ok(isDeleted);
        }
    }
}