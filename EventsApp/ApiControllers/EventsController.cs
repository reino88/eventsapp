﻿using EventsApp.Events;
using EventsApp.JQueryDatatable;
using System.Configuration;
using System.Web.Http;

namespace EventsApp
{
    /// <summary>
    /// Api controllers for events
    /// </summary>
    [RoutePrefix("api/Events")]
    public class EventsController : ApiController
    {
        private static readonly string _connectionString = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;

        /// <summary>
        /// HttpPost Get upcoming evnts from db by DatatableRequest object
        /// </summary>
        /// <param name="request">Datatable request parameters</param>
        /// <returns>New DataTableResponse object</returns>
        [HttpPost]
        [Route("GetUpcomingEvents")]
        public DataTableResponse GetUpcomingEvents(DatatableRequest request)
        {
            return new DataTableResponse
            {
                draw = request.Draw,
                data = EventsAccessLayer.GetUpcomingEvents(
                    request.Start + 1, out int recordsTotal, request.Length).ToArray(),
                recordsTotal = recordsTotal,
                recordsFiltered = recordsTotal,
                error = ""
            };
        }

        /// <summary>
        /// HttpPost Get past events from db by DatatableRequest request object
        /// </summary>
        /// <param name="request">Datatable request parameters</param>
        /// <returns>IHttpActionResult Ok(DataTableResponse response)</returns>
        [Route("GetPastEvents")]
        [HttpPost]
        public IHttpActionResult GetPastEvents(DatatableRequest request)
        {
            DataTableResponse response = new DataTableResponse()
            {
                draw = request.Draw,
                data = EventsAccessLayer.GetPastEvents(
                    request.Start + 1, out int recordsTotal, request.Length).ToArray(),
                recordsTotal = recordsTotal,
                recordsFiltered = recordsTotal,
                error = "",
            };
            return Ok(response);
        }

        /// <summary>
        /// HttpPost Insert event to db
        /// </summary>
        /// <param name="clientEvent">ClientEvent to insert</param>
        /// <returns>IHttpActionResult Ok(true) if event already exists, else Ok(false)</returns>
        [HttpPost]
        [Route("InsertEvent")]
        public IHttpActionResult InsertEvent([FromBody]ClientEvent clientEvent)
        {
            bool isInEvent;
            EventsAccessLayer.InsertClientEvent(clientEvent, out isInEvent);

            return Ok(isInEvent);
        }

        /// <summary>
        /// HttpPost Delete event from db by id
        /// </summary>
        /// <param name="eventId">Event Id to delete</param>
        /// <returns>If deleted returns IHttpActionResult (true), else IHttpActionResult(false)</returns>
        [Route("DeleteClientEvent/{eventID}")]
        [HttpPost]
        public IHttpActionResult DeleteEvent(long eventId)
        {
            bool isDeleted;
            EventsAccessLayer.DeleteClientEvent(eventId, out isDeleted);

            return Ok(isDeleted);
        }
    }
}