﻿using EventsApp.Participants;
using EventsApp.Participants.CompanyInfo;
using Newtonsoft.Json.Linq;
using System;
using System.Web.Http;

namespace EventsApp.ApiControllers
{
    /// <summary>
    /// Api controllers for comapny
    /// </summary>
    public class CompanyController : ApiController
    {
        /// <summary>
        /// HttpPost Insert company to event
        /// </summary>
        /// <param name="newParticipant">New participant object</param>
        /// <returns>If company already exists in event returns true, else false</returns>
        [HttpPost]
        [Route("api/InsertCompany")]
        public IHttpActionResult InsertCompany([FromBody]JObject newParticipant)
        {
            long eventId = Convert.ToInt64(newParticipant.SelectToken("newParticipant.eventId"));
            Company company = newParticipant.SelectToken("newParticipant.participant").ToObject<Company>();
            bool isInEvent = false;

            CompanyAccessLayer.InsertCompany(eventId, company, out isInEvent);
            return Ok(isInEvent);
        }

        /// <summary>
        /// HttpPut Update company in event by id
        /// </summary>
        /// <param name="companyId">Company Id to update</param>
        /// <param name="company">Company object with new values</param>
        /// <returns>If company is updated returns true, else false</returns>
        [HttpPut]
        [Route("api/UpdateCompany/{CompanyID}")]
        public IHttpActionResult UpdateCompany(long companyId, [FromBody]Company company)
        {
            CompanyAccessLayer.UpdateCompany(companyId, company, out bool isUpdated);
            return Ok(isUpdated);
        }

        /// <summary>
        /// HttpPost Delete company from event
        /// </summary>
        /// <param name="data">Data contains event id and participant id</param>
        /// <returns>IHttpActionResult</returns>
        [HttpPost]
        [Route("api/DeleteCompany")]
        public IHttpActionResult DeleteCompany([FromBody]JObject data)
        {
            long eventId = Convert.ToInt64(data["eventId"]);
            long participantId = Convert.ToInt64(data["participantId"]);

            CompanyAccessLayer.DeleteCompanyFromEvent(eventId, participantId, out bool isDeleted);
            return Ok(isDeleted);
        }
    }
}