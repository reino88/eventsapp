﻿using System;

namespace EventsApp
{
    /// <summary>
    /// Master page main class
    /// </summary>
    public partial class Site : System.Web.UI.MasterPage
    {
        /// <summary>
        /// Main method for Master Site
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Eventhandler for click to redirect Home.aspx
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnHomePage_Click(object sender, EventArgs e)
        {
            Response.Redirect("Home.aspx");
        }

        /// <summary>
        ///  Eventhandler for click to redirect AddUserEvent.aspx
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnEventPage_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddUserEvent.aspx");
        }
    }
}