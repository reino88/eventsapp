﻿
namespace EventsApp.Participants
{
    /// <summary>
    /// Class model for Participant in event
    /// </summary>
    public class Participant
    {
        /// <summary>
        /// Get or set Event id where participant participating
        /// </summary>
        public long IdEvent { get; set; }
        /// <summary>
        /// Get or set participant id (database sets Id)
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// Get or set rownumber
        /// </summary>
        public string RowNumber { get; set;}
        /// <summary>
        /// Get or set type of participant 
        /// </summary>
        public string ParticipantType { get; set; }
        /// <summary>
        /// Get or set real life id for participant
        /// </summary>
        public string IdParticipant { get; set; }
        /// <summary>
        /// Get or set participant name
        /// </summary>
        public string Name { get; set; }

    }
}