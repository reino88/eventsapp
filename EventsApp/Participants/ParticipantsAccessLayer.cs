﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace EventsApp.Participants
{
    /// <summary>
    /// Class for accessing methods participants in events
    /// </summary>
    public class ParticipantsAccessLayer
    {
        private static string _connectionString =
           ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;

        /// <summary>
        /// Global constant for default page size to pager
        /// </summary>
        public const int PAGESIZE = 20;

        /// <summary>
        /// <see langword="static"/> method for getting all participants in event
        /// </summary>
        /// <param name="eventId">Event id where to get participant</param>
        /// <param name="start">Witch index to start getting participants </param>
        /// <param name="recordsTotal">Output variable, how much records available in db</param>
        /// <param name="length">Page size</param>
        /// <returns>List of Participants depending on arguments values</returns>
        public static List<Participant> GetAllParticipantsInEvent(long eventId, int start, out int recordsTotal, int length = PAGESIZE)
        {
            List<Participant> participants = new List<Participant>();

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand("usp_Select_AllParticipants_InEvent", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@IdEvent", eventId);
                    command.Parameters.AddWithValue("@Start", start);
                    command.Parameters.AddWithValue("@Length", length);
                    SqlParameter TotalCount = new SqlParameter()
                    {
                        ParameterName = "@RecordsTotal",
                        SqlDbType = SqlDbType.Int,
                        Direction = ParameterDirection.Output
                    };
                    command.Parameters.Add(TotalCount);

                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        Participant participant = new Participant()
                        {
                            IdEvent = Convert.ToInt64(reader["IdEvent"]),
                            ID = reader["ID"].ToString(),
                            RowNumber = reader["RowNumber"].ToString(),
                            ParticipantType = reader["ParticipantType"].ToString(),
                            IdParticipant = reader["IdParticipant"].ToString(),
                            Name = reader["Name"].ToString(),
                        };
                        participants.Add(participant);
                    }
                    connection.Close();
                    recordsTotal = Convert.ToInt32(TotalCount.Value);
                }
                return participants;
            }
        }
    }
}