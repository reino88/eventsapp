﻿using EventsApp.Participants.CompanyInfo;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace EventsApp.Participants
{
    /// <summary>
    /// Class for accessing company
    /// </summary>
    public class CompanyAccessLayer
    {
        private static string _connectionString =
           ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;

        /// <summary>
        /// <see langword="static"/> Insert new company to event
        /// </summary>
        /// <param name="eventId">Event id where to insert company</param>
        /// <param name="company">New company object</param>
        /// <param name="isInEvent">If event already exists returns true, else false/> </param>
        public static void InsertCompany(long eventId, Company company, out bool isInEvent)
        {
            using (SqlConnection _connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand("usp_tblCompany_Insert", _connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@CompanyName", company.Name);
                    command.Parameters.AddWithValue("@RegistryNumber", company.RegistryNumber);
                    command.Parameters.AddWithValue("@CompanyParticipants", company.Participants);
                    command.Parameters.AddWithValue("@CompanyPayment", company.Payment);
                    command.Parameters.AddWithValue("@CompanyAdditionalInfo", company.AdditionalInfo);
                    command.Parameters.AddWithValue("@IdEvent", eventId);

                    _connection.Open();
                    isInEvent = Convert.ToBoolean(command.ExecuteScalar());
                    _connection.Close();
                }
            }
        }

        /// <summary>
        /// <see langword="static"/> method for getting company data by id
        /// </summary>
        /// <param name="companyId">Company id to get company</param>
        /// <returns>DataSet of company</returns>
        public static DataSet GetCompany(long companyId)
        {
            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            DataSet dataSet = new DataSet();

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                dataAdapter = new SqlDataAdapter("usp_tblCompany_Select_by_ID", connection);
                dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                dataAdapter.SelectCommand.Parameters.AddWithValue("@ID", companyId);
                dataSet = new DataSet();
                dataAdapter.Fill(dataSet);
            }
            return dataSet;
        }

        /// <summary>
        /// <see langword="static"/> method for updating company
        /// </summary>
        /// <param name="companyId">Company id to update</param>
        /// <param name="company">New company object</param>
        /// <param name="isUpdated">If company is updated returns true else false</param>
        public static void UpdateCompany(long companyId, Company company, out bool isUpdated)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = new SqlCommand
                {
                    Connection = connection
                };
                command.Parameters.AddWithValue("@ID", companyId);
                command.Parameters.AddWithValue("@Name", company.Name);
                command.Parameters.AddWithValue("@RegistryNumber", company.RegistryNumber);
                command.Parameters.AddWithValue("@Participants", company.Participants);
                command.Parameters.AddWithValue("@Payment", company.Payment);
                command.Parameters.AddWithValue("@AdditionalInfo", company.AdditionalInfo);

                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "usp_tblCompany_Update_By_Id";
                command.Connection.Open();

                isUpdated = Convert.ToBoolean(command.ExecuteScalar());
            }
        }

        /// <summary>
        /// <see langword="static"/> method fot deleting company from event
        /// </summary>
        /// <param name="eventId">Event id where to delete company</param>
        /// <param name="companyId">Company id to delete with</param>
        /// <param name="isDeleted">Out param, if company deleted</param>
        public static void DeleteCompanyFromEvent(long eventId, long companyId, out bool isDeleted)
        {
            using (SqlConnection _connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand("usp_tblCompany_Delete", _connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@IdEvent", eventId);
                    command.Parameters.AddWithValue("@IdCompany", companyId);

                    _connection.Open();
                    isDeleted = Convert.ToBoolean(command.ExecuteScalar());
                }
            }
        }
    }
}