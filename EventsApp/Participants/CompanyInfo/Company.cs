﻿
namespace EventsApp.Participants.CompanyInfo
{
    /// <summary>
    /// Class model for company
    /// </summary>
    public class Company
    {
        /// <summary>
        /// Get or set Company name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Get or set ompanyregistry number
        /// </summary>
        public string RegistryNumber { get; set; }
        /// <summary>
        /// Get or set Company participants in event
        /// </summary>
        public short Participants { get; set; }
        /// <summary>
        /// Get or set Company payment
        /// </summary>
        public string Payment { get; set; }
        /// <summary>
        /// Get or set Company Additional info
        /// </summary>
        public string AdditionalInfo { get; set; }
    }
}