﻿using EventsApp.Participants.PersonInfo;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace EventsApp.Participants
{
    /// <summary>
    /// Class for accessing person
    /// </summary>
    public class PersonAccessLayer
    {
        private static string _connectionString =
           ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;

        /// <summary>
        /// <see langword="static"/> method for inserting person to event
        /// </summary>
        /// <param name="eventId">Event id where to insert person</param>
        /// <param name="person">New person object</param>
        /// <param name="isInEvent">If person already exists in event returns true, else false</param>
        public static void InsertPerson(long eventId, Person person, out bool isInEvent)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand("usp_tblPerson_Insert", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@FirstName", person.FirstName);
                    command.Parameters.AddWithValue("@Surname", person.LastName);
                    command.Parameters.AddWithValue("@PersonalId", person.PersonalID);
                    command.Parameters.AddWithValue("@PersonPayment", person.Payment);
                    command.Parameters.AddWithValue("@PersonAdditionalInfo", person.AdditionalInfo);
                    command.Parameters.AddWithValue("@IdEvent", eventId);

                    connection.Open();
                    isInEvent = Convert.ToBoolean(command.ExecuteScalar());
                    command.Connection.Close();
                }
            }
        }

        /// <summary>
        /// <see langword="static"/> method for getting person from db
        /// </summary>
        /// <param name="personId">Id for getting person</param>
        /// <returns>Dataset of person</returns>
        public static DataSet GetPerson(long personId)
        {
            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            DataSet dataSet = new DataSet();

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                dataAdapter = new SqlDataAdapter("usp_tblPerson_Select_By_ID", connection);
                dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                dataAdapter.SelectCommand.Parameters.AddWithValue("@ID", personId);
                dataSet = new DataSet();
                dataAdapter.Fill(dataSet);
            }
            return dataSet;
        }

        /// <summary>
        /// <see langword="static"/> method for updating person
        /// </summary>
        /// <param name="personId">Id where to update person</param>
        /// <param name="person">New person object to insert</param>
        /// <param name="isUpdated">If person updated returns true, else false</param>
        public static void UpdatePerson(long personId, Person person, out bool isUpdated)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = new SqlCommand
                {
                    Connection = connection
                };
                command.Parameters.AddWithValue("@ID", personId);
                command.Parameters.AddWithValue("@FirstName", person.FirstName);
                command.Parameters.AddWithValue("@Surname", person.LastName);
                command.Parameters.AddWithValue("@PersonalID", person.PersonalID);
                command.Parameters.AddWithValue("@Payment", person.Payment);
                command.Parameters.AddWithValue("@AdditionalInfo", person.AdditionalInfo);

                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "usp_tblPerson_Update";
                command.Connection.Open();
                isUpdated = Convert.ToBoolean(command.ExecuteScalar());
            }
        }

        /// <summary>
        /// <see langword="static"/> method for deleting person from event
        /// </summary>
        /// <param name="eventId">Event id where to delete person</param>
        /// <param name="personId">Person id to delete person</param>
        /// <param name="isDeleted">Out param if person is deleted</param>
        public static void DeletePersonFromEvent(long eventId, long personId, out bool isDeleted)
        {
            using (SqlConnection _connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand("usp_tblPerson_Delete", _connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@IdEvent", eventId);
                    command.Parameters.AddWithValue("@IdPerson", personId);

                    _connection.Open();
                    isDeleted = Convert.ToBoolean(command.ExecuteScalar());
                }
            }
        }
    }
}