﻿
namespace EventsApp.Participants.PersonInfo
{
    /// <summary>
    /// Class model for person
    /// </summary>
    public class Person 
    {
        /// <summary>
        /// Get or set person first name
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Get or set person Last name
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Get or set person personal id
        /// </summary>
        public string PersonalID { get; set; }
        /// <summary>
        /// Get or set person payment
        /// </summary>
        public string Payment { get; set; }
        /// <summary>
        /// Get or set person additional info
        /// </summary>
        public string AdditionalInfo { get; set; }
    }
}