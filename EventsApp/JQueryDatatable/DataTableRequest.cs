﻿
namespace EventsApp.JQueryDatatable
{
    /// <summary>
    /// Class model for jQuery datatable needed request variables
    /// </summary>
    public class DatatableRequest
    {
        /// <summary>
        /// Gets or sets start index to get data
        /// </summary>
        public int Start { get; set; }
        /// <summary>
        /// Gets or sets data length to get
        /// </summary>
        public int Length { get; set; }
        /// <summary>
        /// Gets or sets number of draw
        /// </summary>
        public int Draw { get; set; }
    }
}