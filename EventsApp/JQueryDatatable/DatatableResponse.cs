﻿
namespace EventsApp.JQueryDatatable
{
    /// <summary>
    /// Class model for jQuery datatable needed response variables
    /// </summary>
    public class DataTableResponse
    {
        /// <summary>
        /// Gets or sets number of draw
        /// </summary>
        public int draw { get; set; }
        /// <summary>
        /// Gets or sets total records in table
        /// </summary>
        public long recordsTotal { get; set; }
        /// <summary>
        /// Gets or sets number of filtered records fom table
        /// </summary>
        public int recordsFiltered { get; set; }
        /// <summary>
        /// Gets or sets object array of data
        /// </summary>
        public object[] data { get; set; }
        /// <summary>
        /// Gets or sets error string
        /// </summary>
        public string error { get; set; }
    }
}