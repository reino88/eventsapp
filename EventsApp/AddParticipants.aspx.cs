﻿using EventsApp.Events;
using EventsApp.Helpers;
using EventsApp.Participants;
using EventsApp.Participants.CompanyInfo;
using EventsApp.Participants.PersonInfo;
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EventsApp
{
    /// <summary>
    /// CodeBehind Class for AddParticipants.aspx
    /// </summary>
    public partial class AddParticipants : System.Web.UI.Page
    {
        private string QueryString => Request.QueryString["ID"];
        private long EventId => Convert.ToInt64(this.QueryString);

        //protected void Page_Init(object sender, EventArgs e)
        //{
        //    //EventId = Encription.DecryptedString(HttpUtility.UrlDecode(Request.QueryString["ID"]));
        //}

        /// <summary>
        /// Main method for Participants.aspx
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!this.QueryString.IsNumeric())
                {
                    Response.Redirect("Home.aspx");
                }

                BindGvParticipants();
                DlEventBind();
                RblPerson.Checked = true;
                BtnRadioTogglerA.Attributes.Add("disabled", "disbled");
                Session["eventId"] = this.EventId;
            }
        }

        /// <summary>
        /// Bind Event by id to datalist DlEvent
        /// </summary>
        private void DlEventBind()
        {
            DlEvent.DataSource = EventsAccessLayer.GetEventById(this.EventId);
            DlEvent.DataBind();
        }

        /// <summary>
        /// Insert company into database
        /// </summary>
        private void InsertCompany()
        {
            Company company = new Company()
            {
                Name = TxtCompanyName.Text,
                RegistryNumber = TxtCompanyRegistryNumber.Text,
                Participants = Convert.ToInt16(TxtCompanyParticipants.Text),
                Payment = DdlCompanyPayment.Text,
                AdditionalInfo = TxtCompanyAdditionalInfo.Text
            };

            CompanyAccessLayer.InsertCompany(this.EventId, company, out bool isInEvent);

            //If company exists in db
            LblClientMessage.Text = isInEvent ?
                "Ettevõte juba eksisteerib meie andmebaasis." :
                "Ettevõte lisatud andmebaasi.";
        }

        /// <summary>
        /// Insert person into database
        /// </summary>
        private void InsertPerson()
        {
            Person person = new Person()
            {
                FirstName = TxtPersonFirstName.Text,
                LastName = TxtPersonSurname.Text,
                PersonalID = TxtPersonPersonalId.Text,
                Payment = DdlPersonPayment.Text,
                AdditionalInfo = TxtPersonAdditionalInfo.Text
            };

            PersonAccessLayer.InsertPerson(this.EventId, person, out bool isInEvent);

            LblClientMessage.Text = isInEvent ?
                "Ettevõte juba eksisteerib meie andmebaasis." :
                "Ettevõte lisatud andmebaasi.";
        }

        /// <summary>
        /// Redirect to participant details page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnParticipantDetails_Click(object sender, EventArgs e)
        {
            int rowIndex = ((sender as Button).NamingContainer as GridViewRow).RowIndex;
            string IdParticipant = GvParticipants.DataKeys[rowIndex].Values[0].ToString();
            string participantType = (sender as Button).CommandArgument.ToString();

            HttpCookie cookie = new HttpCookie("ParticipantType")
            {
                Value = participantType
            };
            Response.Cookies.Add(cookie);

            Response.Redirect("ParticipantDetails.aspx?ID=" + IdParticipant);
        }

        /// <summary>
        /// Delete participant from db
        /// </summary>
        /// <param name="sender">GridView</param>
        /// <param name="e">GridViewDeleteEventArgs</param>
        protected void GvParticipants_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridView gridView = sender as GridView;
            int idParticipant = Convert.ToInt32(e.Keys["IdParticipant"]);
            string participantType = e.Keys["ParticipantType"].ToString();
            bool isDeleted = false;
            try
            {
                if (participantType == "Person")
                {
                    PersonAccessLayer.DeletePersonFromEvent(this.EventId, idParticipant, out isDeleted);
                }
                if(participantType == "Company")
                {
                    CompanyAccessLayer.DeleteCompanyFromEvent(this.EventId, idParticipant, out isDeleted);
                }
               
                //if page is empty, change page
                if (gridView.Rows.Count == 1 && gridView.PageIndex != 1)
                {
                    gridView.PageIndex -= 1;
                }
                BindGvParticipants();
                LblClientMessage.Text = String.Empty;
            }
            catch (Exception)
            {
                e.Cancel = true;
                Server.Transfer("~/Error/InternalServerError.aspx");
            }
        }

        /// <summary>
        /// Insert participant to db
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnInsertParticipant_Click(object sender, EventArgs e)
        {
            if (RblPerson.Checked == true)
            {
                Validate("person");

                if (Page.IsValid)
                {
                    InsertPerson();
                }
            }
            if (RblCompany.Checked == true)
            {
                Validate("company");

                if (Page.IsValid)
                {
                    InsertCompany();
                }
            }
            BindGvParticipants();
        }

        /// <summary>
        /// Toggle company insert form.
        /// Clears client message.
        /// </summary>
        /// <remarks>Uses button with radio button</remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadioTogglerA_Click(object sender, EventArgs e)
        {
            RblPerson.RadioButtonToggle(RblCompany);
            PnlInsertCompany.Controls.Clear();
            LblClientMessage.Text = String.Empty;

            Button button = (sender as Button);
            button.Attributes.Add("disabled", "disabled");
            BtnRadioTogglerB.Attributes.Remove("disabled");
        }

        /// <summary>
        /// Toggle person insert form.
        /// Clears client message.
        /// </summary>
        /// <remarks>Uses button with radio button</remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadioTogglerB_Click(object sender, EventArgs e)
        {
            RblCompany.RadioButtonToggle(RblPerson);
            PnlInsertPerson.Controls.Clear();
            LblClientMessage.Text = String.Empty;

            Button button = (sender as Button);
            button.Attributes.Add("disabled", "disabled");
            BtnRadioTogglerA.Attributes.Remove("disabled");
        }

        /// <summary>
        /// Toggle visibility between PnlInsertPerson and PnlInsertCompany
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PnlInsertCompany_PreRender(object sender, EventArgs e)
        {
            if (RblCompany.Checked)
            {
                PnlInsertPerson.ToggleVisibility(PnlInsertCompany);
            }
        }

        /// <summary>
        /// Toggle visibility between PnlInsertPerson and PnlInsertCompany
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PnlInsertPerson_PreRender(object sender, EventArgs e)
        {
            if (RblPerson.Checked)
            {
                PnlInsertCompany.ToggleVisibility(PnlInsertPerson);
            }
        }

        /// <summary>
        /// Bind participants to gridview GvParticipants with pager
        /// </summary>
        private void BindGvParticipants()
        {
            int recordsTotal = 0;
            int pageIndex = GvParticipants.PageIndex;
            int length = ParticipantsAccessLayer.PAGESIZE;
            int startRowNumber = ((pageIndex - 1) * length) + 1;

            //Bind upcoming events
            GvParticipants.DataSource = ParticipantsAccessLayer.GetAllParticipantsInEvent(this.EventId, startRowNumber, out recordsTotal, length);
            GvParticipants.DataBind();

            //Bind pager
            DlPager1.DataSource = Pager.SetPager(recordsTotal, pageIndex, length);
            DlPager1.DataBind();
            Pager.PagerToggler(DlPager1, pageIndex);
        }

        /// <summary>
        /// Set GvParticipants page index to pager command argument value
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void DlPager1_ItemCommand(object source, DataListCommandEventArgs e)
        {
            GvParticipants.PageIndex = Convert.ToInt16(e.CommandArgument);
            LblClientMessage.Text = String.Empty;
            BindGvParticipants();
        }

        /// <summary>
        /// Redirect Home.aspx
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnBackToHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("Home.aspx");
        }
    }
}