﻿<%@  UICulture="et" Culture="et-EE" Title="Ürituse lisamine" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddUserEvent.aspx.cs" Inherits="EventsApp.AddUserEvent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="bg-white pb-5">
        <%--Title and image--%>
        <div class="row text-light mx-0 my-3">
            <div class="col col-sm-5 col-md-4 mx-0 col-lg-3 col-xl-2 bg-blue pr-0">
                <p class="text-align-center h4 px-0 mx-0 py-1 font-weight-light">Ürituse lisamine</p>
            </div>
            <div class="col-12 col-sm px-0">
                <img src="Content/Images/libled.jpg"
                    class="img-full img-full-sm img-full-md img-full-lg img-full-xl"
                    role="img" />
            </div>
        </div>
        <%--/.Title and image--%>

        <%--Label event addings--%>
        <div class="container offset-md-3">
            <div class="row fg-blue">
                <label id="" class="text-left col h5 mb-3">Ürituse lisamine</label>
            </div>

            <%--Event data inputs--%>
            <%--Event Name--%>
            <div class="form-group row">
                <label for="TxtEventName" class="form-label col-sm-3 col-md-3 col-lg-2 text-left">Ürituse nimi:</label>
                <div class="col-11 col-sm-8 col-md-5">
                    <asp:TextBox ID="TxtEventName" 
                        Visible="true"
                        runat="server"
                        MaxLength="40" 
                        CssClass="form-control form-control-sm">

                    </asp:TextBox>
                </div>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorEventName"
                    ControlToValidate="TxtEventName"
                    ValidationGroup="event"
                    Text="*"
                    ErrorMessage="TxtEventName"
                    runat="Server"
                    ForeColor="Red"
                    Font-Bold="true">
                </asp:RequiredFieldValidator>
            </div>
            <%--/.Event Name--%>

            <%--Date & Time--%>
            <div class="form-group row">
                <label for="TxtEventDateTime" class="form-label col-sm-3 col-md-3 col-lg-2 text-left">Toimumisaeg:</label>
                <div class="col-11 col-sm-8 col-md-5">
                    <asp:TextBox ID="TxtEventDateTime"
                        AutoCompleteType="Disabled"
                        Text="pp.kk.aaaa hh:mm"
                        MaxLength="16"
                        runat="server"
                        ForeColor="GrayText"
                        CssClass="form-control form-control-sm">

                    </asp:TextBox>
                </div>
                <asp:CustomValidator ID="CustomValidator1"
                    runat="server"
                    ErrorMessage="CustomValidator"
                    ControlToValidate="TxtEventDateTime"
                    OnServerValidate="ValidateDate"
                    ValidationGroup="event"
                    ClientValidationFunction="validateDate"
                    Text="*"
                    ValidateEmptyText="true"
                    ForeColor="Red"
                    Font-Bold="true"></asp:CustomValidator>
            </div>
            <%--/.Date & Time--%>

            <%--Location--%>
            <div class="form-group row">
                <label for="TxtEventLocation" class="form-label col-sm-3 col-md-3 col-lg-2 text-left">Koht:</label>
                <div class="col-11 col-sm-8 col-md-5">
                    <asp:TextBox ID="TxtEventLocation"
                        runat="server"
                        MaxLength="40"
                        CssClass="form-control form-control-sm"></asp:TextBox>
                </div>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorEventLocation"
                    ControlToValidate="TxtEventLocation"
                    ValidationGroup="event"
                    Text="*"
                    ErrorMessage="TxtEventLocation"
                    runat="Server"
                    ForeColor="Red"
                    Font-Bold="true">
                </asp:RequiredFieldValidator>
            </div>
            <%--/.Location--%>

            <%--Additional info--%>
            <div class="form-group row">
                <label for="TxtEventAdditionalInfo" class="form-label col-sm-3 col-md-3 col-lg-2 text-left">Lisainfo:</label>
                <div class="col-11 col-sm-8 col-md-5">
                    <asp:TextBox ID="TxtEventAdditionalInfo"
                        MaxLength="1000"
                        TextMode="MultiLine"
                        runat="server"
                        CssClass="form-control form-control-sm">

                    </asp:TextBox>
                </div>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorEventAdditionalInfo"
                    ControlToValidate="TxtEventAdditionalInfo"
                    ValidationGroup="event"
                    Text="*"
                    ErrorMessage="TxtEventAdditionalInfo"
                    runat="Server"
                    ForeColor="Red"
                    Font-Bold="true">
                </asp:RequiredFieldValidator>
            </div>
            <%--/.Additional info--%>
        </div>
        <%--/.Event data inputs--%>

        <%--Message for client//--%>
        <asp:Label ID="LblClientMessage"
            runat="server"
            ForeColor="#005aa1"
            CssClass="col offset-md-3 mt-3 mt-3">

        </asp:Label>

        <%--Action buttons--%>
        <div class="col offset-md-3 mt-3">

            <%--Home.aspx//--%>
            <asp:Button ID="BtnBackToHome"
                UseSubmitBehavior="true"
                ClientIDMode="Static"
                OnClientClick="JavaScript: window.history.back(1); return false;"
                OnClick="BtnBackToHome_Click"
                Text="Tagasi"
                runat="server"
                BorderStyle="None"
                CssClass="btn pr-3 pl-3 mr-2" />

            <%--Add data control//--%>
            <asp:Button ID="BtnSave"
                OnClick="BtnSave_Click" 
                UseSubmitBehavior="true"
                ValidationGroup="event"
                CausesValidation="true"
                Text="Lisa"
                runat="server"
                BorderStyle="None"
                CssClass="btn bg-blue text-light pr-4 pl-4" />
        </div>
        <%--/.Action buttons--%>
    </div>

    <link href="Content/jquery.datetimepicker.css" rel="stylesheet" />
    <script src="Scripts/jquery.datetimepicker.full.min.js"></script>
    <script src="Scripts/DateTimePickerConf.js"></script>

</asp:Content>
