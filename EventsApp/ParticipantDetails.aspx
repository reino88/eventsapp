﻿<%@ Page Title="Osalejate detailid" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ParticipantDetails.aspx.cs" Inherits="EventsApp.ParticipantDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="bg-white pb-5">
        <%--Title and image--%>
        <div class="row  text-light mr-0 ml-0 mt-3 mb-3">
            <div class="col col-height col-sm-5 col-md-4 col-lg-3 col-xl-2 bg-blue">
                <p class="text-align-center h4 pt-3 pb-3">Osavõtja info</p>
            </div>
            <div class="col-12 col-sm pl-0 pr-0">
                <img class="img-full img-full-sm img-full-md img-full-lg img-full-xl" src="Content/Images/libled.jpg" />
            </div>
        </div>
        <%--/.Title and image--%>

        <div class="offset-md-3 pl-md-3 w-100 pl-2 pl-sm-4 pr-sm-4 p-md-0">

            <div class="row fg-blue">
                <div class="col-12 col-sm-3 col-md-2 px-0">
                    <label class="text-left col h5 mb-3">
                        Osavõtja info

                    </label>
                </div>
                <div class="col">
                    <asp:Label ID="LblClientMessage"
                        ClientIDMode="Static"
                        runat="server"
                        CssClass="labelClass">

                    </asp:Label>
                </div>

            </div>

            <%--Person--%>
            <asp:DataList ID="DlPerson"
                OnItemDataBound="DlPerson_ItemDataBound"
                OnUpdateCommand="DlPerson_UpdateCommand"
                DataKeyField="ID"
                runat="server" 
                CssClass="w-100">
                <ItemTemplate>

                    <%--FirstName--%>
                    <div class="form-group row">
                        <label for="TxtFirstName" class="form-label col-3 col-md-2 col-xl-1 text-left mr-xl-5">Eesnimi:</label>
                        <div class="col-11 col-sm-8 col-md-5" style="display:block;">

                            <asp:TextBox ID="TxtFirstName"
                                CssClass="form-control form-control-sm"
                                Text='<%# Server.HtmlDecode(Eval("FirstName").ToString()) %>'
                                runat="server"
                                MaxLength="40">

                            </asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RfvTxtFirstName"  
                            ControlToValidate="TxtFirstName"
                            ErrorMessage="RequiredFieldValidator" 
                            ValidationGroup="Person"
                            runat="server"
                            Text="*" 
                            ForeColor="Red"
                            Font-Bold="true"
                            Display="Dynamic"
                            CssClass="px-0 col-1">

                        </asp:RequiredFieldValidator>
                    </div>
                    <%--/.FirstName--%>

                    <%--LastName--%>
                    <div class="form-group row">
                        <label for="TxtSurname" class="form-label col-3 col-md-2 col-xl-1 text-left mr-xl-5">Perenimi:</label>
                        <div class="col-11 col-sm-8 col-md-5">
                            <asp:TextBox ID="TxtSurname"
                                Text='<%# Eval("Surname")%>'
                                runat="server"
                                MaxLength="50" 
                                CssClass="form-control form-control-sm">

                            </asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RfvTxtSurname"
                            ControlToValidate="TxtSurname"
                            ValidationGroup="Person"
                            ErrorMessage="RequiredFieldValidator"
                            runat="server"
                            Text="*"
                            Display="Dynamic"
                            ForeColor="Red"
                            Font-Bold="true"
                            CssClass="px-0 col-1">

                        </asp:RequiredFieldValidator>
                    </div>
                    <%--/.LastName--%>

                    <%--PersonalID--%>
                    <div class="form-group row">
                        <label for="TxtPersonalId" class="form-label col-3 col-md-2 col-xl-1 mr-xl-5 text-left">Isikukood:</label>
                        <div class="col-11 col-sm-8 col-md-5">
                            <asp:TextBox ID="TxtPersonalId"
                                Text='<%# Eval("PersonalID")%>'
                                runat="server"
                                MaxLength="11"
                                CssClass="form-control form-control-sm">

                            </asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RfvTxtPersonalId"  
                            ControlToValidate="TxtPersonalId"
                            ErrorMessage="RequiredFieldValidator" 
                            ValidationGroup="Person"
                            runat="server"
                            Text="*" 
                            ForeColor="Red"
                            Font-Bold="true"
                            Display="Dynamic"
                            CssClass="px-0 col-1">

                        </asp:RequiredFieldValidator>
                         <asp:RegularExpressionValidator
                            ID="RevTxtPersonPersonalId"
                            ControlToValidate="TxtPersonalId"
                            ValidationExpression="^([1-9][0-9]{10,10})$"
                            ValidationGroup="person"
                            ErrorMessage="RevTxtTxtPersonalId"
                            runat="server"
                            Text="*"
                            Display="Dynamic"
                            ForeColor="Red"
                            CssClass="px-0 textBox participantValidator">

                        </asp:RegularExpressionValidator>
                    </div>
                    <%--/.PersonalID--%>

                    <%--PersonPayment--%>
                    <div class="form-group row">
                        <label for="DdlPayment" class="form-label col-3 col-md-2 col-xl-1 mr-xl-5 text-left">Maksmisviis</label>
                        <div class="col-11 col-sm-8 col-md-5">
                            <asp:DropDownList ID="DdlPayment"
                                runat="server"
                                CssClass="form-control form-control-sm">
                                <asp:ListItem Value="Ülekanne" Selected="True">Ülekanne</asp:ListItem>
                                <asp:ListItem Value="Sularaha">Sularaha</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <%--/.Payment--%>

                    <%--AdditionalInfo--%>
                    <div class="form-group row">
                        <label for="TxtAdditionalInfo" class="form-label col-3 col-md-2 col-xl-1 mr-xl-5 text-left">Lisainfo</label>
                        <div class="col-11 col-sm-8 col-md-5">
                            <asp:TextBox ID="TxtAdditionalInfo" 
                                runat="server" 
                                MaxLength="1500" 
                                TextMode="MultiLine"
                                CssClass="form-control form-control-sm">

                            </asp:TextBox>
                        </div>
                    </div>

                    <%--/.AdditionalInfo--%>
                    <asp:Button ID="BtnBack1"
                        OnClick="BtnBack_Click"
                        ClientIDMode="Static"
                        runat="server"
                        CausesValidation="false"
                        Text="Tagasi"
                        BorderStyle="None"
                        CssClass="btn pr-3 pl-3 mr-2" />

                    <asp:Button ID="BtnUpdatePerson"
                        OnClientClick="javascript: updateParticipant();"
                        ClientIDMode="Static"
                        CommandName="update" 
                        ValidationGroup="Person"
                        Text="Salvesta"
                        runat="server" Visible="true"
                        BorderStyle="None" 
                        CssClass="btn bg-blue text-light pr-2 pl-2" />

                </ItemTemplate>
            </asp:DataList>
            <%--/.Person--%>

            <%--Company--%>
            <asp:DataList ID="DlCompany"
                OnItemDataBound="DlCompany_ItemDataBound"
                DataKeyField="ID"
                OnUpdateCommand="DlCompany_UpdateCommand"
                runat="server"
                CssClass="w-100">
                <ItemTemplate>
                    <%--Name--%>
                    <div class="form-group row">
                        <label for="TxtName" class="form-label col-3 col-md-2 col-xl-1 text-left">Nimi:</label>
                        <div class="col-12 col-sm-9 col-md-5">
                            <asp:TextBox ID="TxtName" 
                                runat="server" 
                                MaxLength="50" 
                                CssClass="form-control form-control-sm" 
                                Text='<%# Bind("Name") %>'>

                            </asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RfvTxtName"  
                            ControlToValidate="TxtName"
                            ErrorMessage="RequiredFieldValidator" 
                            ValidationGroup="Company"
                            runat="server"
                            Text="*" 
                            ForeColor="Red"
                            Font-Bold="true"
                            Display="Dynamic"
                            CssClass="px-0 col-1">

                        </asp:RequiredFieldValidator>
                    </div>
                    <%--/.Name--%>

                    <%--RegistryNumber--%>
                    <div class="form-group row">
                        <label for="TxtRegistryNumber" class="form-label col-3 col-md-2 col-xl-1 text-left">Registrinumber:</label>
                        <div class="col-12 col-sm-9 col-md-5">
                            <asp:TextBox ID="TxtRegistryNumber" 
                                runat="server" 
                                MaxLength="40" 
                                CssClass="form-control form-control-sm" 
                                Text='<%# Bind("RegistryNumber") %>'></asp:TextBox>
                        </div>
                          <asp:RequiredFieldValidator ID="RfvTxtRegistryNumber"  
                            ControlToValidate="TxtRegistryNumber"
                            ErrorMessage="RequiredFieldValidator" 
                            ValidationGroup="Company"
                            runat="server"
                            Text="*" 
                            ForeColor="Red"
                            Font-Bold="true"
                            Display="Dynamic"
                            CssClass="col-1 px-0">

                        </asp:RequiredFieldValidator>
                    </div>
                    <%--/.RegistryNumber--%>

                    <%--Participants--%>
                    <div class="form-group row">
                        <label for="TxtParticipants" class="form-label col-3 col-md-2 col-xl-1 text-left">Osalejatearv:</label>
                        <div class="col-12 col-sm-9 col-md-5">
                            <asp:TextBox ID="TxtParticipants" 
                                runat="server" 
                                MaxLength="11" 
                                CssClass="form-control form-control-sm" 
                                Text='<%# Bind("Participants") %>'></asp:TextBox>
                        </div>
                         <asp:RequiredFieldValidator ID="RfvTxtParticipants"  
                            ControlToValidate="TxtParticipants"
                            ErrorMessage="RequiredFieldValidator" 
                            ValidationGroup="Company"
                            runat="server"
                            Text="*" 
                            ForeColor="Red"
                            Font-Bold="true"
                            Display="Dynamic"
                            CssClass="col-1 px-0">

                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RevTxtParticipants" runat="server" 
                            ValidationGroup="Company"
                            ControlToValidate="TxtParticipants"
                            Text="*"
                            ForeColor="Red"
                            ErrorMessage="RequiredFieldValidator" 
                            Display="Dynamic"
                            ValidationExpression="^[1-9][0-9]*$"
                            CssClass="px-0 font-weight-bold">

                        </asp:RegularExpressionValidator>
                    </div>
                    
                    <%--/.Participants--%>

                    <%--Payment--%>
                    <div class="form-group row">
                        <label for="DdlPayment" class="form-label col-3 col-md-2 col-xl-1 text-left">Maksmisviis:</label>
                        <div class="col-12 col-sm-9 col-md-5">
                            <asp:DropDownList ID="DdlPayment" runat="server" CssClass="form-control form-control-sm">
                                <asp:ListItem Value="Ülekanne">Ülekanne</asp:ListItem>
                                <asp:ListItem Value="Sularaha">Sularaha</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <%--/.Payment--%>

                    <%--AdditionalInfo--%>
                    <div class="form-group row">
                        <label for="TxtAdditionalInfo" class="form-label col-3 col-md-2 col-xl-1 text-left">Lisainfo:</label>
                        <div class="col-12 col-sm-9 col-md-5">
                            <asp:TextBox ID="TxtAdditionalInfo"
                                runat="server" 
                                MaxLength="1500" 
                                TextMode="MultiLine" 
                                CssClass="form-control form-control-sm" 
                                Text='<%# Bind("AdditionalInfo") %>'>

                            </asp:TextBox>
                        </div>
                    </div>
                    <%--/.AdditionalInfo--%>
                    <asp:Button ID="BtnBack"
                        OnClick="BtnBack_Click"
                        runat="server"
                        CausesValidation="false" ValidateRequestMode="Disabled"
                        Text="Tagasi"
                        BorderStyle="None"
                        CssClass="btn pr-4 pl-4 mr-2" />

                    <asp:Button ID="BtnUpdateCompany"
                        OnClientClick="javascript: updateParticipant();"
                        CommandName="update"
                        Text="Salvesta"
                        runat="server"
                        BorderStyle="None" CausesValidation="true"
                        CssClass=" btn bg-blue text-light" />
                </ItemTemplate>
            </asp:DataList>
            <%--/.Company--%>
        </div>
    </div>
</asp:Content>
