﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace EventsApp.Events
{
    /// <summary>
    /// Class for accessing events
    /// </summary>
    public class EventsAccessLayer
    {
        private static readonly string _connectionString =
            ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;

        /// <summary>
        /// Default constant for page length.
        /// </summary>
        public const int PageLength = 20;

        /// <summary>
        /// <see langword="static"/>  method for getting upcoming events from db by start index and length
        /// </summary>
        /// <param name="startIndex">Start row number for getting data</param>
        /// <param name="recordsTotal">Outpt param for all records in table</param>
        /// <param name="length">Total rows for output. Default 20</param>
        /// <returns>Returns list of UpcomingEvent</returns>
        public static List<ClientEvent> GetUpcomingEvents(int startIndex, out int recordsTotal, int length = PageLength)
        {
            List<ClientEvent> clientEvents = new List<ClientEvent>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand("usp_tblEvents_Select_UpComingEvent", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@Start", startIndex);
                    command.Parameters.AddWithValue("@Length", length);
                    SqlParameter RecordsTotal = new SqlParameter()
                    {
                        ParameterName = "@RecordsTotal",
                        SqlDbType = SqlDbType.BigInt,
                        Direction = ParameterDirection.Output
                    };
                    command.Parameters.Add(RecordsTotal);

                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        ClientEvent clientEvent = new ClientEvent()
                        {
                            ID = Convert.ToInt64(reader["ID"]),
                            RowNumber = reader["RowNumber"].ToString(),
                            Name = reader["Name"].ToString(),
                            Date = reader["Date"].ToString(),
                            Location = reader["Location"].ToString()
                        };

                        clientEvents.Add(clientEvent);
                    }
                    connection.Close();
                    recordsTotal = Convert.ToInt32(RecordsTotal.Value);
                }
                return clientEvents;
            }
        }

        /// <summary>
        /// <see langword="static"/> method for geting past events from db by startindex, and length
        /// </summary>
        /// <param name="startIndex">Start row number for getting data </param>
        /// <param name="recordsTotal">All records in table</param>
        /// <param name="length">Total rows for output. Default 20</param>
        /// <returns>List of ClientEvent</returns>
        public static List<ClientEvent> GetPastEvents(int startIndex, out int recordsTotal, int length = PageLength)
        {
            List<ClientEvent> clientEvents = new List<ClientEvent>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand("usp_tblEvents_Select_PastEvent", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@Start", startIndex);
                    command.Parameters.AddWithValue("@Length", length);

                    SqlParameter RecordsTotal = new SqlParameter()
                    {
                        ParameterName = "@RecordsTotal",
                        SqlDbType = SqlDbType.BigInt,
                        Direction = ParameterDirection.Output
                    };
                    command.Parameters.Add(RecordsTotal);

                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        ClientEvent clientEvent = new ClientEvent()
                        {
                            ID = Convert.ToInt64(reader["ID"]),
                            RowNumber = reader["RowNumber"].ToString(),
                            Name = reader["Name"].ToString(),
                            Date = reader["Date"].ToString(),
                            Location = reader["Location"].ToString()
                        };
                        clientEvents.Add(clientEvent);
                    }
                    connection.Close();
                    recordsTotal = Convert.ToInt32(RecordsTotal.Value);
                }
                return clientEvents;
            }
        }

        /// <summary>
        /// <see langword="static"/> method for getting event from db by id
        /// </summary>
        /// <param name="eventId">Event Id to get event</param>
        /// <returns>Dataset of events</returns>
        public static DataSet GetEventById(long eventId)
        {
            DataSet dataSet = new DataSet();

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter("usp_tblEvents_Select_Event_By_ID", connection);
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@ID", eventId);
                adapter.Fill(dataSet);
            }
            return dataSet;
        }

        /// <summary>
        /// <see langword="static"/>  method for inserting new event to db
        /// </summary>
        /// <param name="clientEvent">New event object</param>
        /// <param name="isInDatabase">Output param for check if event is already in db</param>
        public static void InsertClientEvent(ClientEvent clientEvent, out bool isInDatabase)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand("usp_tblEvent_Insert", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection.Open();
                    command.Parameters.AddWithValue("@EventName", clientEvent.Name);
                    command.Parameters.AddWithValue("@DateTime", clientEvent.Date);
                    command.Parameters.AddWithValue("@Location", clientEvent.Location);
                    command.Parameters.AddWithValue("@EventAdditionalInfo", clientEvent.AdditionalInfo);
                    isInDatabase = Convert.ToBoolean(command.ExecuteScalar());
                }
            }
        }

        /// <summary>
        /// <see langword="static"/> method for deleting event from db by id
        /// </summary>
        /// <param name="eventId">Event id</param>
        /// <param name="isDeleted">Output param for checking if event is deleted</param>
        public static void DeleteClientEvent(long eventId, out bool isDeleted)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand("usp_tblEvents_Delete", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@ID", eventId);
                    connection.Open();
                    isDeleted = Convert.ToBoolean(command.ExecuteScalar());
                };
            }
        }
    }
}