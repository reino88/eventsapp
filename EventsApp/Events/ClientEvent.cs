﻿
namespace EventsApp.Events
{
    /// <summary>
    /// Class model for event
    /// </summary>
    public class ClientEvent
    {
        /// <summary>
        /// Get or set event id
        /// </summary>
        public long ID { get; set; }
        /// <summary>
        /// Get or set rowNumber
        /// </summary>
        public string RowNumber { get; set; }
        /// <summary>
        /// Get or set event name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Get or set event date 
        /// </summary>
        public string Date { get; set; }
        /// <summary>
        /// Get or set event location
        /// </summary>
        public string Location { get; set; }
        /// <summary>
        /// Get or set event additional info
        /// </summary>
        public string AdditionalInfo { get; set; }
        /// <summary>
        /// Get or set total count over events
        /// </summary>
        public int TotalCount { get; set; }
    }
}