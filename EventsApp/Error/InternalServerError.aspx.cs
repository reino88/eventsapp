﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EventsApp.Error
{
    /// <summary>
    /// This class is only for error view
    /// </summary>
    public partial class InternalServerError : System.Web.UI.Page
    {
        /// <summary>
        /// This method have no extra functionality
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}