﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InternalServerError.aspx.cs" Inherits="EventsApp.Error.InternalServerError" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width" />
    <title>Server Error!</title>
     <link href="../Content/Error/Error.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div id="container">
            <div id="smile">:(</div>
            <span id="p1">500 </span>
            <span class="paragraph-style1">Server Error</span>
            <h2>Oih, Midagi läks valesti.</h2>
            <h3>Värskendage lehte või proovige hiljem uuesti.</h3>
            <br />
            <h3><a href="../Home.aspx">Pealehele</a></h3>
        </div>
    </form>
</body>
</html>
