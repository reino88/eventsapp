﻿using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace EventsApp.Helpers
{
    /// <summary>
    /// <see langword="static"/> class for togglers
    /// </summary>
    public static class Toggler
    {
        /// <summary>
        /// Extension for toggling radio button/check box values whether the button control is checked
        /// </summary>
        /// <param name="thisObject">Object 1 to toggle</param>
        /// <param name="other">Object 2 to toggle</param>
        public static void RadioButtonToggle(this object thisObject, object other)
        {
            if (thisObject is HtmlInputRadioButton && other is HtmlInputRadioButton)
            {
                HtmlInputRadioButton thisInputRadio = thisObject as HtmlInputRadioButton;
                HtmlInputRadioButton otherInputRadio = other as HtmlInputRadioButton;

                if (thisInputRadio.Checked == true)
                {
                    thisInputRadio.Checked = false;
                    otherInputRadio.Checked = true;
                }
                else if (otherInputRadio.Checked == true)
                {
                    thisInputRadio.Checked = true;
                    otherInputRadio.Checked = false;
                }
            }
        }

        /// <summary>
        /// Extension method for Toggling visibility 
        /// </summary>
        /// <param name="thisObject">Object 1 to hide or show</param>
        /// <param name="other">Object 2 to hide or show</param>
        public static void ToggleVisibility(this object thisObject, object other)
        {
            if (thisObject is WebControl && other is WebControl)
            {
                (thisObject as WebControl).Style.Add("display", "none");
                (other as WebControl).Style.Add("display", "inherit");
            }
        }
    }
}