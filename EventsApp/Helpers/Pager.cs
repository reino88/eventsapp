﻿using EventsApp.Events;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EventsApp.Helpers
{
    /// <summary>
    /// <see langword="static"/> class for data paging
    /// </summary>
    public static class Pager
    {
        private const int TOTALBUTTONSINPAGE = 9; //Set total number of page buttons for pager
        private const int LENGTH = EventsAccessLayer.PageLength; //Set total number of rows in page

        static int totalPageCount;
        static int startPageButton;
        static int lastPageButton;

        /// <summary>
        /// <see langword="static"/> method for implementing page navigation. Allows to implement custom paging
        /// </summary>
        /// <param name="totalRowCount">All rows in data</param>
        /// <param name="currentPage">Page number that is active</param>
        /// <param name="length">Data length</param>
        /// <param name="totalButtonsInPage">Total count of page buttons(numbers)</param>
        /// <returns>List of pagenumbers</returns>
        public static List<ListItem> SetPager(int totalRowCount, int currentPage = 0, int length = LENGTH, int totalButtonsInPage = TOTALBUTTONSINPAGE)
        {
            totalPageCount = (int)Math.Ceiling((decimal)totalRowCount / length);
            startPageButton = Math.Max(currentPage - (int)Math.Floor((decimal)totalButtonsInPage / 2), 1);
            lastPageButton = Math.Min(startPageButton + totalButtonsInPage - 1, totalPageCount);

            if ((startPageButton + totalButtonsInPage - 1) > totalPageCount)
            {
                lastPageButton = Math.Min(currentPage + (int)Math.Floor((decimal)totalButtonsInPage / 2), totalPageCount);
                startPageButton = Math.Max(lastPageButton - totalButtonsInPage + 1, 1);
            }

            //Generate pager
            List<ListItem> buttonsContainer = new List<ListItem>();
            if (startPageButton != 1)
            {
                buttonsContainer.Add(new ListItem("Esimene", "1"));
            }

            for (int i = startPageButton; i <= lastPageButton; i++)
            {
                buttonsContainer.Add(new ListItem(i.ToString(), i.ToString()));
            }

            if (lastPageButton != totalPageCount)
            {
                buttonsContainer.Add(new ListItem("Viimane", totalPageCount.ToString()));
            }

            //Hide pager if only one page
            if(buttonsContainer.Count == 1)
            {
                buttonsContainer[0].Text = String.Empty;
            }
           
            return buttonsContainer;
        }

        /// <summary>
        /// <see langword="static"/> method for switching and disabling pager buttons
        /// </summary>
        /// <param name="dataList">Parent container of toggler</param>
        /// <param name="currentPage">Active pager page</param>
        public static void PagerToggler(DataList dataList, int currentPage)
        {
            foreach (DataListItem items in dataList.Controls)
            {
                foreach (Control control in items.Controls)
                {
                    if (control is Button button)
                    {
                        Int16.TryParse(button.CommandArgument.ToString(), out short pageNumber);

                        if (pageNumber == currentPage)
                        {
                            button.Attributes.Add("disabled", "disabled");
                        }
                    }
                }
            }
        }
    }
}