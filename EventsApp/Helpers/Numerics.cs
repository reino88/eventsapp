﻿using System;
using System.Linq;

namespace EventsApp.Helpers
{
    /// <summary>
    /// <see langword="static"/> Class for helper functions
    /// </summary>
    public static class Numerics
    {
        /// <summary>
        /// Extension methot for checking if input string is numeric
        /// </summary>
        /// <param name="number">Input string to check if number</param>
        /// <returns></returns>
        public static bool IsNumeric(this string number)
        {
            return !string.IsNullOrEmpty(number) && number.All(Char.IsDigit);
        }
    }
}