﻿
//Set jQuery DateTimePicker 
jQuery.datetimepicker.setLocale('et');

function setDateTimePicker(textboxId) {
    $(textboxId).datetimepicker({
        step: 5,
        format: 'd.m.Y H:i',
        minDate: 0,
        dayOfWeekStart: + 1,
        i18n: {
            et: {
                months: [
                    'Jaanuar', 'Veebruar', 'Märts', 'Aprill',
                    'Mai', 'Juuni', 'Juuli', 'August',
                    'September', 'Oktoober', 'November', 'Detsember',
                ],
                dayOfWeek: [
                    "E", "T", "K", "N",
                    "R", "L", "P"
                ]
            }
        }
    });
}

setDateTimePicker('[id$="TxtEventDateTime"]');