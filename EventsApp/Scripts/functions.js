﻿
$(function () {

    const navItem = $('.nav-item'),
        toggler = $("#navbarToggler1"),
        lblClientMessage = $('[id$="LblClientMessage"]'),
        btnRadioToggler = $('#BtnRadioTogglerA');

    //Toggle navbar ../Home.aspx
    toggler.click(function () {
        navItem.toggle();
    });

    //Clear message for client on document click, if no error
    $(this).click(function () {
        const ajaxError = lblClientMessage.find('.error');

        if (!ajaxError.length > 0) {
            lblClientMessage.text('');
        }
    });

    // Enable first radio button in ../AddParticipants.aspx
    btnRadioToggler.prop("disabled", "");

    //Bind GridViews
    bindGvUpcomingEvents();
    bindGvPastEvents();
    bindGvParticipants();
});

/**
 * 
 * @param {string} Name
 * @param {string} Date
 * @param {string} Location
 * @param {string} AdditionalInfo
 * @returns {ClientEvent}
 */
function ClientEvent(Name, Date, Location, AdditionalInfo) {
    this.Name = Name;
    this.Date = Date;
    this.Location = Location;
    this.AdditionalInfo = AdditionalInfo;
}

/**
 * @param {string} FirstName
 * @param {string} LastName
 * @param {string} PersonalID
 * @param {string} Payment
 * @param {string} AdditionalInfo
 * @returns {Person}
 */
function Person(FirstName, LastName, PersonalID, Payment, AdditionalInfo) {
    this.FirstName = FirstName;
    this.LastName = LastName;
    this.PersonalID = PersonalID;
    this.Payment = Payment;
    this.AdditionalInfo = AdditionalInfo;
}

/**
 * @param {string} Name
 * @param {string} RegistryNumber
 * @param {string} Participants
 * @param {string} Payment
 * @param {string} AdditionalInfo
 * @returns {Company}
 */
function Company(Name, RegistryNumber, Participants, Payment, AdditionalInfo) {
    this.Name = Name;
    this.RegistryNumber = RegistryNumber;
    this.Participants = Participants;
    this.Payment = Payment;
    this.AdditionalInfo = AdditionalInfo;
}

/**
 * Represents helping methods for project
 * */
function HelperFunctions() {

    /**
     * Set input checked propertie to true by id
     * @param {string} id Id of input(radio buttons/checkbox)
     */
    this.setSelectorChecked = function setSelectorChecked(id) {
        $("input[id=" + id + "]").prop('checked', true);
    };

    /**
     * Toggle between two html element by id
     * @param {string} element1 Id of first element
     * @param {string} element2 Id of second element
     */
    this.toggleElements = function (element1, element2) {

        element1 = $("[id=" + element1 + "]");
        element2 = $("[id=" + element2 + "]");

        element1.show();
        element2.hide();
        return false;
    };

    /**
     * Get parameter from url
     * @param {string} param Parmeter name from url
     */
    this.GetUrlParameterValues = function (param) {

        const url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');

        for (var i = 0; i < url.length; i++) {
            let urlparam = url[i].split('=');
            if (urlparam[0] == param) {
                return urlparam[1];
            }
        }
    };
}

/**
 * Validate function for date and time
 * Format: pp.kk.aaaa hh:mm (day, month, year, hour, minute)
 * @param {any} sender
 * @param {any} args
 */
function validateDate(sender, args) {

    let inputDate = document.getElementById(sender.controltovalidate).value,
        day = inputDate.substring(0, 2),
        month = parseInt(inputDate.substring(3, 5)),
        year = inputDate.substring(6, 10),
        hour = inputDate.substring(11, 13),
        minute = inputDate.substring(14, 16),
        dateString = new Date(year, month - 1, day, hour, minute),
        now = Date.now();

    args.IsValid = inputDate != 'pp.kk.aaaa hh:mm' && dateString > now;
}

/**
 * Get default ajax message or set custom message
 * @param {string} message ajax output message
 */
function ajaxMessage(message) {

    let div = document.createElement("div");

    let background = document.createElement('div');
    $(background).addClass('ajax-background-whitesmoke-1');

    let wrapper = document.createElement("div");
    $(wrapper).addClass('ajax-wrapper-1');

    let messageWrapper = document.createElement('h3');
    $(messageWrapper).addClass('ajax-wrapper-1__text');

    let ellipsis = document.createElement('img');
    ellipsis.setAttribute('src', 'Content/Images/Ellipsis-2.7s-36px.gif');

    $(background).appendTo(div);
    $(messageWrapper).appendTo(wrapper);
    $(ellipsis).appendTo(messageWrapper);
    $(wrapper).insertAfter(background);

    switch (message) {
        case "error":
            $(messageWrapper).append('Tekkis tõrge').addClass('error');
            break;
        case "loading":
            $(messageWrapper).append('Laeb').addClass('loading');
            break;
        case "timeout":
            $(messageWrapper).append('Sessioon on aegunud').addClass('timedOut');
            break;
        default: return message;
    }
    return div;
}

/**
 * Represents functions for ajax calls
 * */
function AjaxCalls() {

    /**
     * Insert client event to db
     * @param {object} clientEvent 
     */
    this.insertClientEvent = function (clientEvent) {

        return $.ajax({
            url: '/api/Events/InsertEvent/',
            method: 'post',
            dataType: 'json',
            data: JSON.stringify(clientEvent),
            contentType: "application/json"
        });
    };

    /**
     * Delete client event from db
     * @param {string|number} eventId
     */
    this.deleteClientEvent = function (eventId) {

        return $.ajax({
            url: 'api/Events/DeleteClientEvent/' + eventId,
            dataType: 'json',
            method: 'post',
            contentType: 'application/json; charset=UTF-8'
        });
    };

    /**
     * Insert participant into client event
     * @param {string|number} eventId whitch event to assign participant
     * @param {string} participantType type of participant
     * @param {object} participant new participant object
     */
    this.insertParticipant = function (eventId, participantType, participant) {
        switch (participantType) {
            case "person":
                return $.ajax({
                    url: 'api/InsertPerson/',
                    method: 'post',
                    dataType: 'json',
                    contentType: 'application/json;charset=utf-8',
                    data: JSON.stringify({ newParticipant: { eventId: eventId, participant: participant } })
                });
            case "company":
                return $.ajax({
                    url: 'api/InsertCompany/',
                    method: 'post',
                    dataType: 'json',
                    contentType: 'application/json;charset=utf-8',
                    data: JSON.stringify({ newParticipant: { eventId: eventId, participant: participant } })
                });

        }
    };

    /**
     * Delete participant from client event
     * @param {string|number} eventId
     * @param {string|number} participantId
     * @param {string|number} IdParticipant
     */
    this.deleteParticipant = function (eventId, participantId, participantType) {

        if (participantType == 'Person') {
            return $.ajax({
                url: 'api/DeletePerson',
                method: 'post',
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify({ eventId: eventId, participantId: participantId })
            });
        }
        if (participantType == 'Company') {
            return $.ajax({
                url: 'api/DeleteCompany',
                method: 'post',
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify({ eventId: eventId, participantId: participantId })
            });
        }
    };

    /**
     * Update person in db
     * @param {string|number} personID
     * @param {object} person
     */
    this.updatePerson = function (personID, person) {
        return $.ajax({
            url: 'api/UpdatePerson/' + personID,
            method: 'put',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(person)
        });
    };

    /**
     * Update company in db
     * @param {string|number} companyID
     * @param {object} company
     */
    this.updateCompany = function (companyID, company) {
        return $.ajax({
            url: 'api/UpdateCompany/' + companyID,
            method: 'put',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(company)
        });
    };
}

/**
 * Set default behavior for jQuery datatables
 * */
function DataTableDefaults() {

    /**
     * Set proccessing message
     * @param {string} table Table name
     */
    this.processing = function (table) {
        $(table).on('processing.dt', function (e, settings, processing) {
            setTimeout(function () {
                processing ?
                    $(table + '_processing').html(
                        ajaxMessage("loading")) :
                    $(table + '_processing').html("");
            }, 500);
        });
    };

    /**
     * Set language options 
     **/
    this.setLanguage = function () {
        return {
            paginate: {
                first: "Esimene",
                previous: "Eelmine",
                next: "Järgmine",
                last: "Viimane"
            },
            processing: ajaxMessage("loading"),
            emptyTable: ""
        };
    };

    /**
     * Row deleteing function for server-side deletion 
     * @param {function} callback
     * @param {string} table
     */
    this.delete = function (callback, table) {
        callback.done(function (response) {
            if (response == true) {
                table.row().remove().draw(false);
            }
        }).fail(function () {
            setTimeout(function () {
                $('[id$=LblClientMessage]').html(ajaxMessage("error"));
            }, 500);
        });
    };
}

/**
 * Custom pager for jQuery datatable
 * @param {any} page
 * @param {any} pages
 */
$.fn.DataTable.ext.pager.full_numbers_no_ellipses = function (page, pages) {

    page = page + 1; //startr number is 0

    const totalButtonsInPage = 9; //Total page numbers in page
    let numbers = [], //Page numbers

        //Calculate start and last button 
        startPageButton = Math.max(page - Math.floor(totalButtonsInPage / 2), 1),
        lastPageButton = Math.min(startPageButton + totalButtonsInPage - 1, pages);

    //Calculate new positions for buttons
    if ((startPageButton + totalButtonsInPage - 1) > pages) {
        lastPageButton = Math.min(page + Math.floor(totalButtonsInPage / 2), pages);
        startPageButton = Math.max(lastPageButton - totalButtonsInPage + 1, 1);
    }
    //Set first pager button
    if (startPageButton != 1) {
        numbers.push("first");
    }
    //Set numbers for pager
    for (let i = startPageButton - 1; i < lastPageButton; i++) {
        numbers.push(i);
    }
    //Set last pager button 
    if (lastPageButton != pages) {
        numbers.push("last");
    }
    //If pager have only one page, hide page number
    if (numbers.length == 1) {
        numbers = [];
    }
    return numbers;
};

/**
 * Bind jQuery datatable to GvUpcomingEvents ../Home.aspx
 * */
function bindGvUpcomingEvents() {

    const ajaxCalls = new AjaxCalls(),
        dataTableDefaults = new DataTableDefaults();

    let table = $("#GvUpcomingEvents").DataTable({
        serverSide: true,
        pageLength: 5,
        searching: false,
        lengthChange: false,
        info: false,
        processing: true,
        pagingType: 'full_numbers_no_ellipses',
        language: dataTableDefaults.setLanguage(),
        ajax: {
            url: '../api/Events/GetUpcomingEvents/',
            method: 'post',
            error: function () {
                setTimeout(function () {
                    $('#GvUpcomingEvents_processing').html(ajaxMessage("error"));
                }, 500);
            }
        },
        createdRow: function (row, data, dataIndex, cells) {
            $(row).addClass('rowEvents w-100 m-0 row text-center text-md-left pb-2 item-hover');
        },
        columns: [
            {
                data: "Name",
                className: 'event-name col col-md-6 col-lg mr-md-2 break-word'
            },
            {
                data: "Date", className: 'eventDate col-12 col-md-3 col-lg-3 p-lg-0'
            },
            {
                data: "ID",
                render: function () {
                    return '<div>' +
                        '<input type="submit" value="Osavõtjad" class="btnMoveToParticipants btn-link" style="color:#6C757D;border-style:None;font-weight:bold;width:90px;">' +
                        '<input type="image" title="Kustuta üritus" class="p-0 btnDeleteEvent"  src="Content/Images/remove.svg" style="color:#6C757D;font-weight:bold;height:15px;">' +
                        '</div>';
                },
                className: 'eventAction col-12 col-md-2 col-lg-3 mr-lg-3 mr-xl-0 p-md-0'
            }
        ],
        columnDefs: [{
            targets: [0, 2],
            createdCell: function (td, cellData, rowData, rowIndex, colIndex) {
                let eventId = cellData;

                switch (colIndex) {
                    case 0:
                        //.text() removes html entities
                        $(td).text(rowData.RowNumber + '. ' + rowData.Name);
                    case 2:
                        $(td).find('.btnMoveToParticipants').on('click', function () {
                            window.location.assign('../AddParticipants.aspx?ID=' + eventId);
                            event.preventDefault();
                        });

                        $(td).find('.btnDeleteEvent').on('click', function () {
                            dataTableDefaults.delete(ajaxCalls.deleteClientEvent(eventId), table);
                            event.preventDefault();
                        });
                    default:
                        return;
                }
            }

        }]
    });

    //Replace server-side pager with jQuery datatable pager
    $('[id$=DlPager1]').replaceWith($('#GvUpcomingEvents_paginate').addClass('text-center'));
    $(table.table().header()).hide();
    dataTableDefaults.processing('#GvUpcomingEvents');
}

/**
 * Bind jQuery datatable to GvPastEvents ../Home.aspx
 * */
function bindGvPastEvents() {

    const dataTableDefaults = new DataTableDefaults();
    let table = $("#GvPastEvents").DataTable({
        serverSide: true,
        pageLength: 5,
        searching: false,
        lengthChange: false,
        info: false,
        processing: true,
        pagingType: 'full_numbers_no_ellipses',
        language: dataTableDefaults.setLanguage(),
        ajax: {
            url: '../api/Events/GetPastEvents',
            method: "post",
            error: function () {
                setTimeout(function () {
                    $('#GvPastEvents_processing').html(ajaxMessage("error"));
                }, 500);
            }
        },
        createdRow: function (row) {
            $(row).addClass('rowEvents w-100 m-0 row text-center text-md-left pb-2 item-hover');
        },
        columns: [
            {
                data: 'Name',
                className: 'event-name col col-md-6 col-lg mr-md-2 break-word'
            },
            {
                data: "Date",
                className: 'eventDate col-12 col-md-3 col-lg-3 p-lg-0'
            },
            {
                data: "ID",
                render: function () {
                    return '<input type="submit" value="Osavõtjad" class="btnMoveToParticipants btn-link" style="color:#6C757D;border-style:None;font-weight:bold;width:90px;">';
                },
                className: 'eventAction col-12 col-md-2 col-lg-3 mr-lg-3 mr-xl-0 p-md-0'
            }
        ],
        columnDefs: [{
            targets: [0, 2],
            createdCell: function (td, cellData, rowData, rowIndex, colIndex) {
                let eventId = cellData;
                switch (colIndex) {
                    case 0:
                        //.text() removes html entities
                        $(td).text(rowData.RowNumber + ". " + rowData.Name);
                    case 2:
                        $(td).find('.btnMoveToParticipants').on('click', function () {
                            window.location.assign('../AddParticipants.aspx?ID=' + eventId);
                            event.preventDefault();
                        });
                    default:
                        return;
                }
            }
        }]
    });

    //Replace server-side pager with jQuery datatable pager
    $('[id$=DlPager2]').replaceWith($('#GvPastEvents_paginate').addClass('text-center'));
    $(table.table().header()).addClass('d-none');
    dataTableDefaults.processing('#GvPastEvents');
}

/**
 * Bind jQuery datatable to GvParticipants ../AddParticipants.aspx
 * */
function bindGvParticipants() {

    const helpers = new HelperFunctions(),
        dataTableDefaults = new DataTableDefaults(),
        ajaxCalls = new AjaxCalls(),
        GvParticipants = $('#GvParticipants');

    let eventId = helpers.GetUrlParameterValues("ID"),
        table = GvParticipants.DataTable({
            serverSide: true,
            searching: false,
            lengthChange: false,
            processing: true,
            info: false,
            language: dataTableDefaults.setLanguage(),
            pagingType: 'full_numbers_no_ellipses',
            ajax: {
                url: "../api/GetAllParticipantsInEvent/" + eventId,
                method: 'post',
                dataType: 'json',
                contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                error: function () {
                    $('#GvParticipants_processing').html(ajaxMessage("error"));
                }
            },

            createdRow: function (row, data, dataIndex, cells) {
                $(row).addClass("row text-left item-hover");
            },

            columns: [
                {
                    data: "Name",
                    className: "participantName col-12 col-sm col-md-4 col-lg-5 mr-md-3 break-word-1"
                },
                {
                    data: "ID",
                    className: "participantID col-12 col-sm pr-md-0 ml-md-3 break-word-1 pr-0"
                },
                {
                    data: "ID",
                    render: function (data, type, full, meta) {
                        return '<div>' +
                            '<input type="submit" value="Vaata" class="BtnParticipantDetails mr-2 bg-none" style="color:#6C757D;border-style:None;font-weight:bold;">' +
                            '<label class="accordion-1 m-0 p-0">' +
                            '<div class="accordion-1__div">' +
                            '<span class="cursor font-weight-bold" style="color:#6c757d">Kustuta</span>' +
                            '<span class="row mr-2">' +
                            '<input type="checkbox" class="accordion-1__input">' +
                            '<span class="accordion-1__span">' +
                            '<input type="submit" value="Jah" class="BtnDeleteParticipant btn-link  ml-3 mr-2" style="border-style:None;">' +
                            '</span>' +
                            '<span class="btn-link accordion-1__span">Ei</span>' +
                            '</span>' +
                            '</div>' +
                            '</label>' +
                            '</div>';
                    },
                    className: "col mr-0 row ml-1"
                }
            ],
            columnDefs: [{
                targets: [0, 2],
                createdCell: function (td, cellData, rowData, rowIndex, colIndex) {
                    switch (colIndex) {
                        case 0:
                            $(td).text(rowData.Name);
                        case 2:
                            $(td).find('.BtnParticipantDetails').on('click', function () {
                                document.cookie = "ParticipantType=" + rowData.ParticipantType;
                                window.location.assign("ParticipantDetails.aspx?ID=" + rowData.IdParticipant);
                                event.preventDefault();
                            });

                            $(td).find('.BtnDeleteParticipant').on('click', function () {
                                dataTableDefaults.delete(ajaxCalls.deleteParticipant(rowData.IdEvent, rowData.IdParticipant, rowData.ParticipantType), table);
                                event.preventDefault();
                            });
                        default:
                            return;
                    }
                }
            }],
            drawCallback: function (settings) {

                //Replace server-side pager with jQuery datatable pager
                $('[id$=DlPager1]').replaceWith($('#GvParticipants_paginate').addClass('text-center py-3'));
                $(table.table().header()).addClass('d-none');
                GvParticipants.css("width", '100%');
                dataTableDefaults.processing('#GvParticipants');
                GvParticipants.find('.dataTables_empty').text("Andmed puuduvad");
            }
        });
}

/**
 * 
 * @param {object} table Table name where to show new data
 */
function particiPantsInsert(table) {
    table = $(table);
    let ajaxCalls = new AjaxCalls(),
        helpers = new HelperFunctions(),
        eventId = helpers.GetUrlParameterValues('ID'),
        participantType,
        participant,
        validationGroup;
    const clientMessage = $('[id$=LblClientMessage]');


    //if radio button RblPerson is checked then participant type is person
    if ($('[id$=RblPerson]').prop('checked')) {

        participantType = 'person';
        validationGroup = 'person';

        let person = new Person(
            $('[id$=TxtPersonFirstName]').val(),
            $('[id$=TxtPersonSurname]').val(),
            $('[id$=TxtPersonPersonalId]').val(),
            $('[id$=DdlPersonPayment]').val(),
            $('[id$=TxtPersonAdditionalInfo]').val()
        );
        participant = person;
    }
    //If radio button RblCompany is checked then participant type is company
    if ($('[id$=RblCompany]').prop('checked')) {

        participantType = 'company';
        validationGroup = 'company';
        let company = new Company(
            $('[id$=TxtCompanyName]').val(),
            $('[id$=TxtCompanyRegistryNumber]').val(),
            $('[id$=TxtCompanyParticipants]').val(),
            $('[id$=DdlCompanyPayment]').val(),
            $('[id$=TxtCompanyAdditionalInfo]').val()
        );
        participant = company;
    }

    if (Page_ClientValidate(validationGroup)) {
        Promise.resolve(ajaxCalls.insertParticipant(eventId, participantType, participant).done(
            function (data) {
                if (data == false) {
                    $(table).DataTable().ajax.reload();
                    clientMessage.text('Osaleja lisatud üritusele.');
                }
                else {
                    clientMessage.text('Osaleja on juba lisatud üritusele.');
                }
            }
        ));
        event.preventDefault();
    }
}

/**
 * update participant from ../ParticipantDetails.aspx
 * */
function updateParticipant() {

    const clientMessage = $('[id$=LblClientMessage]'),
        ajaxCalls = new AjaxCalls(),
        helpers = new HelperFunctions();
    let participantID = helpers.GetUrlParameterValues('ID'),
        participantType = document.cookie.replace(/(?:(?:^|.*;\s*)ParticipantType\s*\=\s*([^;]*).*$)|^.*$/, "$1"),
        validationGroup;

    if (participantType == 'Person') {

        let person = new Person(
            FirstName = $('[id*=TxtFirstName]').val(),
            LastName = $('[id*=TxtSurname]').val(),
            PersonalID = $('[id*=TxtPersonalId]').val(),
            Payment = $('[id*=DdlPayment]').val(),
            AdditionalInfo = $('[id*=TxtAdditionalInfo]').val()
        );
        validationGroup = "person";


        if (Page_ClientValidate(validationGroup)) {
            ajaxCalls.updatePerson(participantID, person).done(function (data) {
                clientMessage.text("Andmed on uuendatud.");
            }).fail(function () {
                clientMessage.html(ajaxMessage("error"));
            });

            if ($.active > 0) {
                setTimeout(function () {
                    clientMessage.html(ajaxMessage("loading"));
                }, 5);
            }
        }
    }

    if (participantType == 'Company') {
        let company = new Company(
            Name = $('[id*=TxtName]').val(),
            RegistryNumber = $('[id*=TxtRegistryNumber]').val(),
            Participants = $('[id*=TxtParticipants]').val(),
            Payment = $('[id*=DdlPayment]').val(),
            AdditionalInfo = $('[id*=TxtAdditionalInfo]').val()
        );
        validationGroup = "company";

        if (Page_ClientValidate(validationGroup)) {
            ajaxCalls.updateCompany(participantID, company).done(function (data) {
                if (data) {
                    clientMessage.text("Andmed on uuendatud.");
                }
            }).fail(function () {
                clientMessage.html(ajaxMessage("error"));
            });

            if ($.active > 0) {
                setTimeout(function () {
                    clientMessage.html(ajaxMessage("loading"));
                }, 5);
            }
        }
    }
    event.preventDefault();
}

/**
 * 
 * @param {any} clientEvent
 */
function insertClientEvent(clientEvent) {

    let ajaxCalls = new AjaxCalls();
    const validationGroup = 'event',
        clientMessage = $('[id$="LblClientMessage"]');

    if (Page_ClientValidate(validationGroup)) {

        ajaxCalls.insertClientEvent(clientEvent).done(function (data) {
            if (data == true) {
                clientMessage.text('Selline üritus juba eksisteerib meie andmebaasis.');
            }
            if (data == false) {
                clientMessage.text('Teie andmed on sisestatud.');
            }
        });
    }
    event.preventDefault();
}

$(function () {
    const btnSave = $('[id$="BtnSave"]');

    btnSave.on('click', function () {
        let eventName = $('[id$="TxtEventName"]').val(),
            dateTime = $('[id$="TxtEventDateTime"]').val(),
            location = $('[id$="TxtEventLocation"]').val(),
            additionalInfo = $('[id$="TxtEventAdditionalInfo"]').val(),
            clientEvent = new ClientEvent(
                eventName,
                dateTime,
                location,
                additionalInfo
            );
        insertClientEvent(clientEvent);
        event.preventDefault();
    });
});

/**
 * Toggle participant forms in ../AddParticipants.aspx
 * @param {any} buttonId
 * @param {any} element1
 * @param {any} element2
 */
function ToggleParticipantForm(buttonId, element1, element2) {

    const helpers = new HelperFunctions(),
        participantValidators = $('.participantValidator'),
        textBoxParticipant = $('.textBoxParticipant');

    helpers.setSelectorChecked(buttonId);
    helpers.toggleElements(element1, element2);

    participantValidators.hide();
    textBoxParticipant.val('');

    event.preventDefault();
}