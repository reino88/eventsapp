
USE DB_Events;
								--TESTS
--INSERT EVENT
/*
	Insert statement
	First 4 statements should be false only when executed first time(events are not in database).	
	Otherwise true(events already are in database).
*/

--Base event
EXECUTE usp_tblEvent_Insert '�ritus 1', '01.01.2020 10:10', 'Asukoht 1', 'Info 1';
GO

--Different name
EXECUTE usp_tblEvent_Insert '�ritus 2', '01.01.2020 10:10', 'Asukoht 1', 'Info 1';
GO

--Different time
EXECUTE usp_tblEvent_Insert '�ritus 1', '02.01.2020 10:10', 'Asukoht 1', 'Info 2';
GO

--Different location
EXECUTE usp_tblEvent_Insert '�ritus 1', '01.01.2020 10:10', 'Asukoht 2', 'Info 3';
GO

/*
	Should generate Conversion failed when converting date and/or time from character string.
	Wrong date and/or time.
*/
EXECUTE usp_tblEvent_Insert '�ritus 1', '32.01.2020 10:10', 'Asukoht 1', 'Info 1';
GO

EXECUTE usp_tblEvent_Insert '�ritus 1', '', 'Asukoht 1', 'Info 1';
GO

EXECUTE usp_tblEvent_Insert '�ritus 1', 's.01.2020 10:10', 'Asukoht 1', 'Info 1';
GO

/*
	Should generate check constraint error.
	Data can't be empty string
*/
EXECUTE usp_tblEvent_Insert '', '3.01.2020 10:10', 'Asukoht 1', 'Info 1';
GO

EXECUTE usp_tblEvent_Insert '�ritus 1', '3.01.2020 10:10', '', 'Info 1';
GO

EXECUTE usp_tblEvent_Insert '�ritus 1', '3.01.2020 10:10', 'Asukoht 1', '';
GO


--SELECT EVENT BY ID

--Should return event with id
EXECUTE usp_tblEvents_Select_Event_By_ID 1

/*
	SELECT UPCOMING EVENTS
	All tests should return only events that is upcoming
*/

--If events exists, should select minus one element because row 0 does not exist
EXECUTE usp_tblEvents_Select_UpComingEvent 0, 3, 1;
GO

--Should Select first 3 event if exists
EXECUTE usp_tblEvents_Select_UpComingEvent 1, 3, 1;
GO

--Should Select 3 event since the third event if exists
EXECUTE usp_tblEvents_Select_UpComingEvent 3, 3, 1;
GO

--Should Select first 100 event if exists
EXECUTE usp_tblEvents_Select_UpComingEvent 1, 100, 1;
GO

/*
	SELECT PAST EVENTS
	All tests should return only events that is past
*/

--If events exists, should select minus one element because row 0 does not exist
EXECUTE usp_tblEvents_Select_PastEvent 0, 3, 1;
GO

--Should Select first 3 event if exists
EXECUTE usp_tblEvents_Select_PastEvent 1, 3, 1;
GO

--Should Select 3 event since the third event if exists
EXECUTE usp_tblEvents_Select_PastEvent 3, 3, 1;
GO

--Should Select first 100 event if exists
EXECUTE usp_tblEvents_Select_PastEvent 1, 100, 1;
GO

--SELECT ALL EVENTS

--If events exists, should select minus one element because row 0 does not exist
EXECUTE usp_tblEvents_Select_AllEvents 0, 3, 1;
GO

--Should Select first 3 event if exists
EXECUTE usp_tblEvents_Select_AllEvents 1, 3, 1;
GO

--Should Select 3 event since the third event if exists
EXECUTE usp_tblEvents_Select_AllEvents 3, 3, 1;
GO

--Should Select first 100 event if exists
EXECUTE usp_tblEvents_Select_AllEvents 1, 100, 1;
GO

--DELETE EVENT

--Count before deleting
SELECT COUNT(*) from tblEvents;

DECLARE @testID bigint = 2;

--Test if event with ID @testID exists
SELECT * from tblEvents where ID = @testID;

/*
	If event with id @testID exists, should delete event and return 1 as deleted.
	Otherwise should return 0 as deleted.
*/
EXECUTE usp_tblEvents_Delete 100;

DECLARE @testID1 bigint = 2;

--Count after deleting
SELECT COUNT(*) from tblEvents;

--Test if event with id @testID exists
SELECT * from tblEvents where ID = @testID1;


--PERSON
--Base Person. 
--If executing first time: should insert new person to tblPerson and return 0 as IsInEvent. 
--Otherwise should return 1 as IsInEvent.
EXECUTE usp_tblPerson_Insert 'FirstName 1', 'SurName 1', '23456789012', '�lekanne', 'AdditionalInfo 1', 3;
GO

--Different name: should return 1 as IsInEvent.
EXECUTE usp_tblPerson_Insert 'FirstName 2', 'SurName 1', '23456789012', '�lekanne', 'AdditionalInfo 1', 3;
GO

--Different surname: should return 1 as IsInEvent.
EXECUTE usp_tblPerson_Insert 'FirstName 1', 'SurName 2', '23456789012', '�lekanne', 'AdditionalInfo 1', 3;
GO

--Different personal ID: should insert new person to tblPerson and return 0 as IsInEvent.
EXECUTE usp_tblPerson_Insert 'FirstName 1', 'SurName 1', '21098765432', '�lekanne', 'AdditionalInfo 1', 3;
GO

--Different payment: should return 1 as IsInEvent.
EXECUTE usp_tblPerson_Insert 'FirstName 1', 'SurName 1', '23456789012', 'Sularaha', 'AdditionalInfo 1', 3;
GO

--Different additional info: should return 1 as IsInEvent.
EXECUTE usp_tblPerson_Insert 'FirstName 1', 'SurName 1', '23456789012', '�lekanne', 'AdditionalInfo 2', 3;
GO

--Different event id: If executing first time: should insert new person to tblPerson and return 0 as IsInEvent. 
--Otherwise should return 1 as IsInEvent.
EXECUTE usp_tblPerson_Insert 'FirstName 1', 'SurName 1', '23456789012', '�lekanne', 'AdditionalInfo 2', 3;
GO

--Wrong payment: Payment have to be "�lekanne" or "Sularaha". 
--Should return error.
EXECUTE usp_tblPerson_Insert 'FirstName 1', 'SurName 1', '23456789012', 'Payment', 'AdditionalInfo 1', 3;
GO

--GET PERSON BY ID
--Should return person with id if exists
EXECUTE usp_tblPerson_Select_By_ID 1;
GO

--UPDATE PERSON
--Also updates tblPersonParticipating
DECLARE @testID bigint = 2;

--Before update
SELECT * FROM tblPerson WHERE ID = @testID;
SELECT * FROM tblPersonParticipating WHERE IdPerson = @testID;

--Should update person by ID 
EXECUTE usp_tblPerson_Update @testID, 'updated firstName2', 'updated Surname', '1999999999922', '�lekanne', 'updated addditionalInfo';

--If payment is not '�lekanne' or 'Sularaha', should return error.
EXECUTE usp_tblPerson_Update @testID, 'updated firstName2', 'updated Surname', '1999999999922', 'payment', 'updated addditionalInfo';

--After update
SELECT * FROM tblPerson WHERE ID = @testID;
SELECT * FROM tblPersonParticipating WHERE IdPerson = @testID;

--DELETE PERSON
--Also deletes from tblPersonParticipating

--Count before deleting
SELECT COUNT(*) from tblPerson;
SELECT COUNT(*) from tblPersonParticipating;
 
SET @testID = 1;

--Test if person with ID @testID exists
SELECT * FROM tblPerson WHERE ID = @testID;
SELECT * FROM tblPersonParticipating WHERE IdPerson = @testID;

/*
	If person with id @testID exists, should delete person from tblPerson and tblPersonParticipating
	and return 1 as deleted.
	Otherwise should return 0 as deleted.
*/
EXECUTE usp_tblPerson_Delete 3, 2

DECLARE @testID1 bigint = 2;

--Count after deleting
SELECT COUNT(*) from tblPerson;
SELECT COUNT(*) from tblPersonParticipating;

--Test if person with id @testID exists
SELECT * from tblPerson WHERE ID = @testID1;
SELECT * from tblPersonParticipating WHERE IdPerson = @testID1;
GO

--INSERT COMPANY

--Base Company. 
--If executing first time: should insert new company to tblCompany and return 0 as IsInEvent. 
--Otherwise should return 1 as IsInEvent.
EXECUTE usp_tblCompany_Insert 'Company 1', 'Reg.No 1', 2,'�lekanne', 'Additional info 1', 3;
GO

--Different name: should return 1 as IsInEvent.
EXECUTE usp_tblCompany_Insert 'Company 2', 'Reg.No 1', 2,'�lekanne', 'Additional info 1', 3;
GO

--Different RegistryNumber: should insert new company to tblPerson and return 0 as IsInEvent.
EXECUTE usp_tblCompany_Insert 'Company 1', 'Reg.No 2', 2,'�lekanne', 'Additional info 1', 3;
GO

--Different participants: should return 1 as IsInEvent.
EXECUTE usp_tblCompany_Insert 'Company 1', 'Reg.No 1', 3,'�lekanne', 'Additional info 1', 3;
GO

--Different payment: should return 1 as IsInEvent.
EXECUTE usp_tblCompany_Insert 'Company 1', 'Reg.No 1', 2,'�lekanne', 'Additional info 1', 3;
GO

--Different additional info: should return 1 as IsInEvent.
EXECUTE usp_tblCompany_Insert 'Company 1', 'Reg.No 1', 2,'�lekanne', 'Additional info 1', 3;
GO

--Different event id: If executing first time: should insert new company to tblCompany and return 0 as IsInEvent. 
--Otherwise should return 1 as IsInEvent.
EXECUTE usp_tblCompany_Insert 'Company 1', 'Reg.No 1', 2,'�lekanne', 'Additional info 1', 4;
GO

--Wrong payment: Payment have to be "�lekanne" or "Sularaha". 
--Should return error.
EXECUTE usp_tblCompany_Insert 'Company 1', 'Reg.No 1', 2,'payment', 'Additional info 1', 3;
GO

--SELECT COMPANY BY ID
--Should return company with id if exists
EXECUTE usp_tblCompany_Select_by_ID 2;
GO

--UPDATE COMPANY
--Also updates tblCompanyParticipating
DECLARE @testID bigint = 2;

--Before update
SELECT * FROM tblCompany WHERE ID = @testID;
SELECT * FROM tblCompanyParticipating WHERE IdCompany = @testID;

--Should update Company by ID
--If payment is not '�lekanne' or 'Sularaha', should return error.
EXECUTE usp_tblCompany_Update_By_Id @testID, 'updated company', 'updated Reg.No', 2, '�ekanne', 'updated addditionalInfo';

--If payment is not '�lekanne' or 'Sularaha', should return error.
EXECUTE usp_tblCompany_Update_By_Id @testID, 'updated company', 'updated Reg.No', 2, 'payment', 'updated addditionalInfo';

--After update
SELECT * FROM tblPerson WHERE ID = @testID;
SELECT * FROM tblPersonParticipating WHERE IdPerson = @testID;
GO

--DELETE COMPANY
--Also deletes from tblCompanyParticipating
--Count before deleting
SELECT COUNT(*) from tblCompany;
SELECT COUNT(*) from tblCompanyParticipating;

DECLARE @testID bigint = 2;

--Test if event with ID @testID exists
SELECT * FROM tblCompany WHERE ID = @testID;
SELECT * FROM tblCompanyParticipating WHERE IdCompany = @testID;

/*
	If company with id @testID exists, should delete company from tblCompany and tblCompanyParticipating
	and return 1 as deleted.
	Otherwise should return 0 as deleted.
*/
EXECUTE usp_tblCompany_Delete 3, 2

DECLARE @testID1 bigint = 2;

--Count after deleting
SELECT COUNT(*) from tblCompany;
SELECT COUNT(*) from tblCompanyParticipating;

--Test if event with id @testID exists
SELECT * from tblCompany where ID = @testID1;
SELECT * from tblCompanyParticipating where IdCompany = @testID1;