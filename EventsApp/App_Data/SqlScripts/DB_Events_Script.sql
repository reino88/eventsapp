
CREATE DATABASE DB_Events;
GO

USE DB_Events;
GO

						--CREATE TABLES

--TABLE EVENTS
CREATE TABLE tblEvents
(
	ID bigint IDENTITY(1,1) NOT NULL,
	Name nvarchar(50) NOT NULL,
	DateTime nvarchar(16) NOT NULL ,
	Location nvarchar(50) NOT NULL ,
	AdditionalInfo nvarchar(1000) NOT NULL,
	CONSTRAINT PK_tblEvents_ID PRIMARY KEY CLUSTERED(ID)
);
GO

ALTER TABLE tblEvents
ADD CONSTRAINT CK_tblEvents_Name CHECK (Name <> '');
GO

ALTER TABLE tblEvents
ADD CONSTRAINT CK_tblEvents_Location CHECK (Location <> '');
GO

ALTER TABLE tblEvents
ADD CONSTRAINT CK_tblEvents_AdditionalInfo CHECK (AdditionalInfo <> '');
GO

--TABLE PERSON
CREATE TABLE tblPerson
(
	ID bigint IDENTITY(1,1) NOT NULL,
	FirstName nvarchar(50) NOT NULL,
	Surname nvarchar(50) NOT NULL,
	PersonalID nvarchar(11) NOT NULL,
	Payment nvarchar(8) NOT NULL,
	AdditionalInfo nvarchar(1500),
	ParticipantType nvarchar(6),
	CONSTRAINT PK_tblPerson_ID PRIMARY KEY CLUSTERED(ID)
);
GO

ALTER TABLE tblPerson
ADD CONSTRAINT CK_tblPerson_FirstName CHECK (FirstName <> '');
GO

ALTER TABLE tblPerson
ADD CONSTRAINT CK_tblPerson_SurName CHECK (Surname <> '');
GO

ALTER TABLE tblPerson
ADD CONSTRAINT CK_tblPerson_PersonalID CHECK (PersonalID <> '');
GO

ALTER TABLE tblPerson
ADD CONSTRAINT DF_tblPerson_Payment DEFAULT '�lekanne' FOR Payment;
GO


ALTER TABLE tblPerson
ADD CONSTRAINT DF_tblPerson_ParticipantType DEFAULT 'Person' FOR ParticipantType;
GO

--TABLE COMPANY
CREATE TABLE tblCompany
(
	ID bigint IDENTITY(1,1) NOT NULL,
	Name nvarchar(50) NOT NULL,
	RegistryNumber nvarchar(50) NOT NULL,
	Participants smallint NOT NULL,
	Payment nvarchar(8) NOT NULL,
	AdditionalInfo varchar(5000),
	ParticipantType nvarchar(7) NOT NULL,
	CONSTRAINT PK_tblCompany_ID PRIMARY KEY CLUSTERED(ID)
);
GO

ALTER TABLE tblCompany
ADD CONSTRAINT CK_tblCompany_Name CHECK (Name <> '');
GO

ALTER TABLE tblCompany
ADD CONSTRAINT CK_tblCompany_RegistryNumber CHECK (RegistryNumber <> '');
GO

ALTER TABLE tblCompany
ADD CONSTRAINT CK_tblCompany_Participants CHECK (Participants <> '');
GO

ALTER TABLE tblCompany
ADD CONSTRAINT DF_tblCompany_Payment DEFAULT '�lekanne' FOR Payment;
GO

ALTER TABLE tblCompany
ADD CONSTRAINT DF_tblCompany_ParticipantType DEFAULT 'Company' FOR ParticipantType;
GO

--TABLE PERSON PARTICIPATING
CREATE TABLE tblPersonParticipating
(
	IdEvent bigint NOT NULL,
	IdPerson bigint NOT NULL,
	PersonName nvarchar(100),
	PersonID nvarchar(11),
	ParticipantType nvarchar(6),
	CONSTRAINT PK_tblPersonParticipating_IdEvents PRIMARY KEY CLUSTERED(IdEvent,IdPerson)
);
GO

ALTER TABLE tblPersonParticipating
ADD CONSTRAINT FK__tblPersonParticipating__IdEvent
FOREIGN KEY (IdEvent) REFERENCES tblEvents(ID);
GO

ALTER TABLE tblPersonParticipating
ADD CONSTRAINT FK__tblPersonParticipating__IdPerson
FOREIGN KEY (IdPerson) REFERENCES tblPerson(ID);
GO

--TABLE COMPANY PARTICIPATING
CREATE TABLE tblCompanyParticipating
(
	IdEvent bigint NOT NULL,
	IdCompany bigint NOT NULL,
	CompanyName nvarchar(50),
	CompanyRegistryNumber nvarchar(50),
	ParticipantType nvarchar(7),
	CONSTRAINT PK_tblCompanyParticipating_IdEvent PRIMARY KEY CLUSTERED (IdEvent, IdCompany)
);
GO

ALTER TABLE tblCompanyParticipating
ADD CONSTRAINT FK__tblCompanyParticipating__IdEvent
FOREIGN KEY (IdEvent) REFERENCES tblEvents(ID);
GO

ALTER TABLE tblCompanyParticipating
ADD CONSTRAINT FK__tblPersonParticipating__IdCompany
FOREIGN KEY (IdCompany) REFERENCES tblCompany(ID);
GO

						--EVENTS PROCEDURES
--INSERT EVENT
CREATE PROCEDURE usp_tblEvent_Insert

@EventName nvarchar(50),
@DateTime nvarchar(16),
@Location nvarchar(50),
@EventAdditionalInfo nvarchar(1000)

AS
BEGIN
	SET NOCOUNT ON;
	SET LANGUAGE estonian;
	SET DATEFORMAT dmy;
	
	DECLARE @IdEvent int;
	DECLARE @IsInDatabase bit; 
	
	IF(ISDATE(@DateTime) = 0)
		THROW 50001, 'Conversion failed when converting date and/or time from character string.', 16;
	
	--If event already exists, use existing event ID
	SELECT @IdEvent = ID 
	FROM tblEvents 
	WHERE Name = @EventName AND
			DateTime = @DateTime AND
			Location = @Location;

	IF (@IdEvent IS NULL)
	BEGIN
		INSERT INTO tblEvents 
		VALUES(@EventName, @DateTime, @Location, @EventAdditionalInfo);

		SET @IsInDatabase = 0;
	END
	ELSE 
		SET @IsInDatabase = 1;

	SELECT @IsInDatabase AS IsInDatabse;
END;
GO

--SELECT EVENT BY ID
CREATE PROCEDURE usp_tblEvents_Select_Event_By_ID

@ID bigint

AS
BEGIN
	SELECT * FROM tblEvents
	WHERE @ID = ID;
END;
GO

--SELECT UPCOMING EVENTS BY START INDEX AND RECORDS LENGTH
CREATE PROCEDURE usp_tblEvents_Select_UpComingEvent

@Start int,
@Length int,
@RecordsTotal int OUTPUT

AS
BEGIN
	SET LANGUAGE estonian;

	SELECT @RecordsTotal = COUNT(*) 
	FROM tblEvents 
	WHERE DateTime > GETDATE();
	WITH CTE AS
	(
	   SELECT TOP(@Start + @Length - 1) ROW_NUMBER() OVER(ORDER BY ID) RowNumber, Name AS Name, DateTime AS Date, Location, ID 
	   FROM tblEvents 
	   WHERE DateTime > GETDATE()
	)
	SELECT * 
	FROM CTE 
	WHERE RowNumber BETWEEN @Start AND (@Start + @Length-1);
END;
GO

--SELECT PAST EVENTS BY START INDEX AND RECORDS LENGTH
CREATE PROCEDURE usp_tblEvents_Select_PastEvent

@Start INT,
@Length INT,
@RecordsTotal INT OUTPUT

AS
BEGIN
	SET LANGUAGE estonian;

	SELECT @RecordsTotal = COUNT(*) 
	FROM tblEvents 
	WHERE DateTime < GETDATE();
	WITH CTE AS
	(
	   SELECT TOP(@Start + @Length - 1) ROW_NUMBER() OVER(ORDER BY ID) RowNumber, Name AS Name, DateTime AS Date, Location, ID 
	   FROM tblEvents 
	   WHERE DateTime < GETDATE()
	)
	SELECT * 
	FROM CTE 
	WHERE RowNumber BETWEEN @Start AND (@Start + @Length - 1);
END;
GO

--SELECT ALL EVENTS BY START INDEX AND RECORDS LENGTH
CREATE PROCEDURE usp_tblEvents_Select_AllEvents

@Start int,
@Length int,
@RecordsTotal int OUTPUT

AS
BEGIN
	SELECT @RecordsTotal = COUNT(*) 
	FROM tblEvents;
	WITH CTE AS
	(
	   SELECT TOP(@Start + @Length - 1) ROW_NUMBER() OVER(ORDER BY ID) RowNumber, Name AS Name, DateTime AS Date, Location, ID 
	   FROM tblEvents
	)
	SELECT * 
	FROM CTE 
	WHERE RowNumber BETWEEN @Start AND (@Start + @Length - 1)
END
GO

--DELETE EVENT BY ID
CREATE PROCEDURE usp_tblEvents_Delete

@ID int

AS
BEGIN	

	SET NOCOUNT ON;

	DECLARE @IsDeleted bit;

	DELETE FROM tblCompanyParticipating WHERE IdEvent = @ID;
	DELETE FROM tblPersonParticipating WHERE IdEvent = @ID;
	DELETE FROM tblEvents WHERE ID = @ID;

	IF((SELECT @@ROWCOUNT) > 0)
	BEGIN
		SET @IsDeleted = 1;
		SELECT ID FROM tblEvents;
	END
	ELSE
		SET @IsDeleted = 0;

	SELECT @IsDeleted AS IsDeleted;
END;
GO

--INSERT PERSON
CREATE PROCEDURE usp_tblPerson_Insert

@FirstName nvarchar(50),
@SurName nvarchar(50),
@PersonalId nvarchar(11),
@PersonPayment nvarchar(8),
@PersonAdditionalInfo nvarchar(1500) = '',
@IdEvent bigint

AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @IdPerson int;

	DECLARE @errorMessage nvarchar(26) = CONCAT('Payment can�t be "', @PersonPayment, '".');
	IF(@PersonPayment NOT IN ('�lekanne', 'Sularaha'))
		THROW 50002, @errorMessage, 16;

	IF NOT EXISTS(SELECT ID FROM tblEvents WHERE ID = @IdEvent)
		SELECT NULL;
	ELSE
	BEGIN
		-- If person already exists, use the existing person ID
		SELECT @IdPerson = ID 
		FROM tblPerson 
		WHERE PersonalID = @PersonalId;
		
		--if person not exixts, add new person and add to event
		IF(@IdPerson IS NULL) 
		BEGIN
			SELECT 0 AS IsInDatabase;

			INSERT INTO tblPerson (FirstName, 
								   Surname, 
								   PersonalID, 
								   Payment, 
								   AdditionalInfo)
			VALUES(@FirstName, 
				   @SurName, 
				   @PersonalId, 
				   @PersonPayment, 
				   @PersonAdditionalInfo);

			SELECT @IdPerson = SCOPE_IDENTITY();

			INSERT INTO tblPersonParticipating 
			VALUES (@IdEvent, 
					@IdPerson, 
					CONCAT(@FirstName, ' ', @SurName), 
					@PersonalId,  
					(SELECT ParticipantType FROM tblPerson WHERE ID = @IdPerson));
		END;
		ELSE
			BEGIN
			
			--if person already exists, add person to another event
			IF NOT EXISTS((SELECT IdPerson 
							FROM tblPersonParticipating 
							WHERE IdEvent = @IdEvent and IdPerson = @IdPerson))
			BEGIN
				SELECT 0 AS IsInEvent;
				INSERT INTO tblPersonParticipating 
				VALUES (@IdEvent, 
					@IdPerson, 
					CONCAT(@FirstName, ' ', @SurName), 
					@PersonalId,  
					(SELECT ParticipantType FROM tblPerson WHERE ID = @IdPerson));
			END;
			ELSE
			BEGIN
				SELECT 1 AS IsInEvent;
			END;
		END;
	END;
END;
GO

--SELECT PERSON by ID
CREATE PROCEDURE usp_tblPerson_Select_By_ID

@ID bigint

AS
BEGIN 
	SELECT * FROM tblPerson
	WHERE ID = @ID;
END
GO

--UPDATE PERSON BY ID
CREATE PROCEDURE usp_tblPerson_Update

@ID bigint,
@FirstName nvarchar(50),
@Surname nvarchar(50),
@PersonalId nvarchar(11),
@Payment nvarchar(8),
@AdditionalInfo nvarchar(1500) = ''

AS
BEGIN

SET NOCOUNT ON;
	DECLARE @IsUpdated bit;

	BEGIN TRY
		BEGIN TRANSACTION
		select * from tblPersonParticipating
			UPDATE tblPerson
			SET FirstName = @FirstName,
				Surname = @Surname,
				PersonalID = @PersonalId,
				Payment = @Payment,
				AdditionalInfo = @AdditionalInfo
				WHERE ID = @ID;
		
			UPDATE tblPersonParticipating
			SET PersonName = CONCAT(@FirstName, ' ', @Surname ),
				PersonID = @PersonalId
				WHERE IdPerson = @ID;

			IF((SELECT @@ROWCOUNT) > 0)
				SET @IsUpdated = 1;
			ELSE 
				SET @IsUpdated = 0;

			SELECT @IsUpdated AS IsUpdated;
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		SET @IsUpdated = 0;
		ROLLBACK TRANSACTION;
	END CATCH;

	SELECT @IsUpdated AS IsUpdated;
END;
GO

--DELETE PERSON
CREATE PROCEDURE usp_tblPerson_Delete

@IdEvent bigint, 
@IdPerson bigint

AS
BEGIN
	DECLARE @isDeleted BIT = 0;

	BEGIN TRY
		BEGIN TRANSACTION
			DELETE tblPersonParticipating
			WHERE IdEvent = @IdEvent AND 
					IdPerson = @IdPerson;
   
			DELETE FROM tblPerson
			WHERE ID = @IdPerson;

			IF (@@ROWCOUNT > 0)
				SET @isDeleted = 1;

		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		SET @isDeleted = 0;
		ROLLBACK TRANSACTION;
	END CATCH;

	SELECT @isDeleted AS isDeleted;
END;
GO

--INSERT COMPANY 
CREATE PROCEDURE usp_tblCompany_Insert

@CompanyName nvarchar(50),
@RegistryNumber nvarchar(50),
@CompanyParticipants smallint,
@CompanyPayment nvarchar(8),
@CompanyAdditionalInfo varchar(5000) = '',
@IdEvent bigint

AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @IdCompany int;

	DECLARE @errorMessage nvarchar(26) = CONCAT('Payment can�t be "', @CompanyPayment, '".');
	IF(@CompanyPayment NOT IN ('�lekanne', 'Sularaha'))
		THROW 50002, @errorMessage, 16;

	--if event Id not exists
	IF NOT EXISTS(SELECT ID FROM tblEvents WHERE ID = @IdEvent)
		SELECT NULL;
	ELSE
	BEGIN

		-- If the company already exists, use the existing company ID
		SELECT @IdCompany = ID 
		FROM tblCompany 
		WHERE RegistryNumber = @RegistryNumber; 

		--if company not exixts, add new company and add to event
		IF (@IdCompany IS NULL)
		BEGIN
			SELECT 0 AS IsInEvent;

			INSERT INTO tblCompany(Name, 
								   RegistryNumber, 
								   Participants, 
								   Payment, 
								   AdditionalInfo)
			VALUES(@CompanyName, 
				   @RegistryNumber,
				   @CompanyParticipants, 
				   @CompanyPayment, 
				   @CompanyAdditionalInfo);

			SELECT @IdCompany = SCOPE_IDENTITY();

			INSERT INTO tblCompanyParticipating 
			VALUES (@IdEvent,
					@IdCompany, 
					@CompanyName, 
					@RegistryNumber, 
					(SELECT ParticipantType FROM tblCompany WHERE ID = @IdCompany));
		END
		ELSE 
		BEGIN
			--if company already exists, add company to another event
		IF NOT EXISTS((SELECT IdCompany FROM tblCompanyParticipating WHERE IdEvent = @IdEvent AND IdCompany = @IdCompany))
		BEGIN
			SELECT 0 AS IsInEvent;

			INSERT INTO tblCompanyParticipating
			VALUES (@IdEvent,
				@IdCompany, 
				@CompanyName, 
				@RegistryNumber, 
				(SELECT ParticipantType FROM tblCompany WHERE ID = @IdCompany));
		END 
		ELSE
			BEGIN
				SELECT 1 AS IsInEvent;
			END;
		END;
	END;
END;
GO

--SELECT COMPANY BY ID
CREATE PROCEDURE usp_tblCompany_Select_by_ID

@ID bigint

AS
BEGIN 
	SELECT * FROM tblCompany
	WHERE ID = @ID;
END;
GO

--UPDATE COMPANY
--Also updates tblCompanyParticipating
CREATE PROCEDURE usp_tblCompany_Update_By_Id

@ID bigint,
@Name nvarchar(50),
@RegistryNumber nvarchar(50),
@Participants smallint,
@Payment nvarchar(8),
@AdditionalInfo varchar(5000) = ''

AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @IsUpdated bit;

	BEGIN TRY
		BEGIN TRANSACTION

			UPDATE tblCompany
			SET Name = @Name,
				RegistryNumber = @RegistryNumber,
				Participants = @Participants,
				Payment = @Payment,
				AdditionalInfo = @AdditionalInfo
				WHERE ID = @ID;
		
			UPDATE tblCompanyParticipating
			SET CompanyName = @Name,
				CompanyRegistryNumber = @RegistryNumber
				WHERE IdCompany = @ID;
				
			IF((SELECT @@ROWCOUNT) > 0)
				SET @IsUpdated = 1;
			ELSE 
				SET @IsUpdated = 0;

			SELECT @IsUpdated AS IsUpdated;
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		SET @IsUpdated = 0;
		ROLLBACK TRANSACTION;
	END CATCH;

	SELECT @IsUpdated AS IsUpdated;
END;
GO

--Delete company from event
CREATE PROCEDURE usp_tblCompany_Delete 

@IdEvent bigint, 
@IdCompany bigint

AS
BEGIN
	DECLARE @isDeleted BIT = 0;

	BEGIN TRY
		BEGIN TRANSACTION
			DELETE tblCompanyParticipating
			WHERE IdEvent = @IdEvent AND 
					IdCompany = @IdCompany;
   
			DELETE FROM tblCompany
			WHERE ID = @IdCompany
			IF (@@ROWCOUNT > 0)
				SET @isDeleted = 1;
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		SET @isDeleted = 0;
		ROLLBACK TRANSACTION;
	END CATCH;

	SELECT @isDeleted AS isDeleted;
END;
GO

--SELECT ALL PARTICIPANTS
CREATE PROCEDURE usp_Select_AllParticipants_InEvent

@IdEvent bigint,
@Start int,
@Length int,
@RecordsTotal int OUTPUT

AS
BEGIN
	--Declare temporary table for participants
	DECLARE @Table TABLE
	(
		IdEvent	bigint,
		ParticipantType	nvarchar(7),
		IdParticipant int,
		Name nvarchar(100),
		ID	nvarchar(11)
	);

	--Insert person to temporary table
	INSERT INTO @Table (IdEvent, ParticipantType, IdParticipant, Name, ID) 
	SELECT IdEvent,
		   ParticipantType,
		   IdPerson AS IdParticipant, 
		   PersonName, 
		   PersonID
	FROM tblPersonParticipating 
	WHERE IdEvent = @IdEvent;
	
	--Insert company to temporary table
	INSERT INTO @Table(IdEvent, ParticipantType, IdParticipant, Name, ID)
	SELECT IdEvent,
		   ParticipantType,
		   IdCompany AS IdParticipant, 
		   CompanyName AS Name, 
		   CompanyRegistryNumber AS ID
	FROM tblCompanyParticipating
	WHERE IdEvent = @IdEvent;
	
	--Select total rows from temporary table
	SELECT @RecordsTotal = COUNT(*) 
	FROM @Table 
	WHERE @IdEvent = IdEvent;
	WITH CTE AS
	(
	   --Select values from temporary table to CTE
	   SELECT  ROW_NUMBER() OVER(ORDER BY ID) RowNumber, 
			  IdEvent, 
			  ParticipantType, 
			  IdParticipant,
			  Name, 
			  ID 
	   FROM @Table 
	   WHERE @IdEvent = IdEvent
	)
	--Select values to output
	SELECT * 
	FROM CTE 
	WHERE RowNumber BETWEEN @Start AND (@Start + @Length - 1);
END;
GO


select * from tblEvents
			--DEMO DATA
--EVENTS
EXECUTE usp_tblEvent_Insert 'Jookseme koos', '11.11.2019 11:10', 'Kuusalu', 'Tulge k�ik.';
EXECUTE usp_tblEvent_Insert 'Raudmehed', '12.01.2019 15:10', 'Tallinn', 'Viimased mehed';
EXECUTE usp_tblEvent_Insert 'Laulge kaasa', '12.12.2018 15:30', 'Haljala', 'Laaalaaalaaa';
EXECUTE usp_tblEvent_Insert 'Mega Meel', '10.10.2019 20:30', 'R��ma Hotell', 'IT ja  muu'

EXECUTE usp_tblEvent_Insert 'Laulge kaasa', '12.12.2017 15:30', 'Haljala', 'Laaalaaalaaa';
EXECUTE usp_tblEvent_Insert 'Mega Meel', '10.10.2017 20:30', 'R��ma Hotell', 'IT ja  muu'

--Person
EXECUTE usp_tblPerson_Insert 'Maario', 'Maasikas', '12345678901', '�lekanne', '-', 1;
EXECUTE usp_tblPerson_Insert 'Maario', 'Maasikas', '12345678901', '�lekanne', 'Maario on ka siin.', 3;
EXECUTE usp_tblPerson_Insert 'Kari', 'Kuusk', '3559452586', 'Sularaha', '', 3;
EXECUTE usp_tblPerson_Insert 'Kari', 'Kuusk', '3559452586', 'Sularaha', '', 4;
EXECUTE usp_tblPerson_Insert 'Kari', 'Kuusk', '3559452586', 'Sularaha', 'Ilus poiss', 1;
EXECUTE usp_tblPerson_Insert 'Liina', 'Kuusk', '236587412569', '�lekanne', 'Tore Neiu.', 1;

--Company
EXECUTE usp_tblCompany_Insert 'Uus Maa', 'H3328Jj', 4, '�lekanne', 'Uusim rajoon.', 1;
EXECUTE usp_tblCompany_Insert 'Dj Ma ja sa', 'jklmnop', 4, '�lekanne', '', 2;
EXECUTE usp_tblCompany_Insert 'Kuu', 'Kuu 24', 4, '�lekanne', '', 3;
EXECUTE usp_tblCompany_Insert '�� ja m�ts', 'Tee12', 4, 'Sularaha', 'Uusim rajoon.', 4;
EXECUTE usp_tblCompany_Insert 'Uus Maa', 'H3328Jj', 4, '�lekanne', 'Uusim rajoon.', 2;
