﻿using EventsApp.Events;
using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EventsApp
{
    /// <summary>
    /// CodeBehind Class for AddUserEvent.aspx
    /// </summary>
    public partial class AddUserEvent : System.Web.UI.Page
    {
        private static string _connectionString = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        private DateTime Now => DateTime.Now;
        private DateTime _dateTime;
        private bool IsBiggerThanNow => _dateTime > this.Now;

        /// <summary>
        /// Main method for AddUserEvent.aspx
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Button btnEventPage = (Button)Master.FindControl("BtnEventPage");
            btnEventPage.BackColor = ColorTranslator.FromHtml("#005AA1");
            btnEventPage.ForeColor = Color.White;
        }

        /// <summary>
        /// Validate Date to be greater than now
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        protected void ValidateDate(object source, ServerValidateEventArgs args)
        {
            DateTime.TryParse(args.Value, out _dateTime);
            args.IsValid = this.IsBiggerThanNow;
        }

        /// <summary>
        /// Insert user event to database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                _dateTime = new DateTime();
                _dateTime = Convert.ToDateTime(TxtEventDateTime.Text);

                ClientEvent clientEvent = new ClientEvent
                {
                    Name = HttpUtility.HtmlDecode(this.TxtEventName.Text),
                    Date = _dateTime.ToString(),
                    Location = TxtEventLocation.Text,
                    AdditionalInfo = TxtEventAdditionalInfo.Text
                };

                bool isInEvent;
                EventsAccessLayer.InsertClientEvent(clientEvent, out isInEvent);

                //Message for user if execute was successful
                LblClientMessage.Text = isInEvent ?
                    "Selline üritus juba eksisteerib meie andmebaasis" :
                    "Teie andmed on sisestatud";
               
            }
        }

        /// <summary>
        /// Method for Redirecting ~/Home.aspx
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnBackToHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("Home.aspx");
        }
    }
}