﻿using EventsApp.Helpers;
using EventsApp.Participants.CompanyInfo;
using EventsApp.Participants.PersonInfo;
using EventsApp.Participants;
using System;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EventsApp
{
    /// <summary>
    /// CodeBehind Class for participantDetails.aspx
    /// </summary>
    public partial class ParticipantDetails : System.Web.UI.Page
    {
        private string _connectionString =
            ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        private string QueryString => Request.QueryString["ID"];
        private string ParticipantType => Request.Cookies["ParticipantType"].Value;
        private string EventId => Session["eventId"].ToString();
        private long ParticipantId => Convert.ToInt64(this.QueryString);
        bool isUpdated;

        /// <summary>
        /// Page load event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //if query string not type Int64, then broken view
                if (!this.QueryString.IsNumeric())
                {
                    Response.Redirect("Home.aspx");
                }
                GetParticipantData();
            }
        }

        /// <summary>
        /// Get peron or company data by participant type
        /// </summary>
        private void GetParticipantData()
        {
            switch (this.ParticipantType)
            {
                case "Person":
                    GetPersonData();
                    return;
                case "Company":
                    GetCompanyData();
                    return;
                default:
                    break;
            }
        }

        /// <summary>
        /// Get person data from db and bind datalist DlPerson
        /// </summary>
        private void GetPersonData()
        {
            DlPerson.DataSource = PersonAccessLayer.GetPerson(this.ParticipantId);
            DlPerson.DataBind();
        }

        /// <summary>
        /// Get company data from db and bind datalist DlCompany
        /// </summary>
        private void GetCompanyData()
        {
            DlCompany.DataSource = CompanyAccessLayer.GetCompany(this.ParticipantId);
            DlCompany.DataBind();
        }

        /// <summary>
        /// Eventhandler forUpdateCommand to update Person  
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void DlPerson_UpdateCommand(object source, DataListCommandEventArgs e)
        {
            int personId = (int)DlPerson.DataKeys[e.Item.ItemIndex];
            TextBox txtFirstName = (TextBox)e.Item.FindControl("TxtFirstName");
            TextBox txtSurname = (TextBox)e.Item.FindControl("TxtSurname");
            TextBox txtPersonalId = (TextBox)e.Item.FindControl("TxtPersonalID");
            DropDownList ddlPayment = (DropDownList)e.Item.FindControl("DdlPayment");
            TextBox txtAdditionalInfo = (TextBox)e.Item.FindControl("TxtAdditionalInfo");

            Person person = new Person()
            {
                FirstName = txtFirstName.Text,
                LastName = txtSurname.Text,
                PersonalID = txtPersonalId.Text,
                Payment = ddlPayment.Text,
                AdditionalInfo = txtAdditionalInfo.Text,
            };

            isUpdated = false;

            Page.Validate();
            if (Page.IsValid)
            {
                PersonAccessLayer.UpdatePerson(personId, person, out isUpdated);

                LblClientMessage.Text = isUpdated ?
                    "Andmed edukalt uuendatud" :
                    "Vabandame, midagi läks valesti.";
            }
            else
            {
                LblClientMessage.Text = "Palun täidke kõik väljad";
            }
        }

        /// <summary>
        /// Eventhandler for UpdateCommand to update Company  
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void DlCompany_UpdateCommand(object source, DataListCommandEventArgs e)
        {
            long companyId = (long)DlCompany.DataKeys[e.Item.ItemIndex];

            TextBox txtName = (TextBox)e.Item.FindControl("TxtName");
            TextBox txtRegistryNumber = (TextBox)e.Item.FindControl("TxtRegistryNumber");
            TextBox txtParticipants = (TextBox)e.Item.FindControl("TxtParticipants");
            DropDownList ddlPayment = (DropDownList)e.Item.FindControl("DdlPayment");
            TextBox txtAdditionalInfo = (TextBox)e.Item.FindControl("TxtAdditionalInfo");

            Company company = new Company()
            {
                Name = txtName.Text,
                RegistryNumber = txtRegistryNumber.Text,
                Participants = Convert.ToInt16(txtParticipants.Text),
                Payment = ddlPayment.Text,
                AdditionalInfo = txtAdditionalInfo.Text,
            };

            Page.Validate("Company");
            if (Page.IsValid)
            {
                CompanyAccessLayer.UpdateCompany(companyId, company, out bool isUpdated);
                LblClientMessage.Text = isUpdated ?
                    "Andmed edukalt uuendatud" :
                    "Palun täidke kõik väljad";
            }
            else
            {
                LblClientMessage.Text = "Palun täidke kõik väljad";
            }
        }

        /// <summary> 
        /// Method for set last cheked value to DdlPayment
        /// </summary>
        /// <param name="e"></param>
        private void SetDdlPaymentValue(DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item ||
                e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DropDownList dropDown = (DropDownList)e.Item.FindControl("DdlPayment");
                dropDown.SelectedValue = ((DataRowView)e.Item.DataItem).Row.ItemArray[4].ToString();
            }
        }

        /// <summary>
        /// Eventhandler for ItemDataBound to set DdlPaymentValue value
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DlCompany_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            SetDdlPaymentValue(e);
        }

        /// <summary>
        /// SetDdlPaymentValue ItemDataBound handler for to set  SetDdlPaymentValue value
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DlPerson_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            SetDdlPaymentValue(e);
        }

        /// <summary>
        /// Redirect to AddParticipants.aspx
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddParticipants.aspx?ID=" + this.EventId);
        }
    }
}