﻿using EventsApp.Events;
using EventsApp.Helpers;
using System;
using System.Configuration;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EventsApp
{
    /// <summary>
    /// CodeBehind Class for Home.aspx
    /// </summary>
    public partial class Home : System.Web.UI.Page
    {
        private static readonly string _connectionString = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        
        /// <summary>
        ///  Main method for Home.aspx
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindGvUpcomingEvents();
                BindGvPastEvents();
            }
            Button btnHomePage = (Button)Master.FindControl("BtnHomePage");
            btnHomePage.BackColor = ColorTranslator.FromHtml("#005AA1");
            btnHomePage.ForeColor = Color.White;
        }

        /// <summary>
        /// Bind upcoming events with pager
        /// </summary>
        private void BindGvUpcomingEvents()
        {
            int length = EventsAccessLayer.PageLength;
            int pageIndex = GvUpcomingEvents.PageIndex;
            int start = ((pageIndex - 1) * length) + 1;
            int recordsTotal = 0;
            GvUpcomingEvents.DataSource = EventsAccessLayer.GetUpcomingEvents(start, out recordsTotal);
            GvUpcomingEvents.DataBind();

            //Bind pager
            DlPager1.DataSource = Pager.SetPager(recordsTotal, pageIndex);
            DlPager1.DataBind();
            Pager.PagerToggler(DlPager1, pageIndex);
        }

        /// <summary>
        /// Bind past events with pager
        /// </summary>
        private void BindGvPastEvents()
        {
            int length = EventsAccessLayer.PageLength;
            int pageIndex = GvPastEvents.PageIndex;
            int start = ((pageIndex - 1) * length) + 1;
            int recordsTotal = 0;

            GvPastEvents.DataSource = EventsAccessLayer.GetPastEvents(start, out recordsTotal);
            GvPastEvents.DataBind();

            //Bind pager
            DlPager2.DataSource = Pager.SetPager(recordsTotal, pageIndex);
            DlPager2.DataBind();
            Pager.PagerToggler(DlPager2, pageIndex);
        }

        /// <summary>
        /// Eventhandler for itemcommant to bind upcoming events by pager page number. 
        /// If event is raised, set page number and bind events
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void DlPager1_ItemCommand(object source, DataListCommandEventArgs e)
        {
            GvUpcomingEvents.PageIndex = Convert.ToInt16(e.CommandArgument);
            BindGvUpcomingEvents();
        }

        /// <summary>
        /// Eventhandler for itemcommand to past events pager
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void DlPager2_ItemCommand(object source, DataListCommandEventArgs e)
        {
            GvPastEvents.PageIndex = Convert.ToInt16(e.CommandArgument);
            BindGvPastEvents();
        }

        /// <summary>
        /// Eventhandler for RowDeleting to bind past events by pager page number. 
        /// If event is raised, set page number and bind events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GvUpcomingEvents_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            long eventId = Convert.ToInt64(e.Keys["ID"]);
            GridView gridView = (GridView)sender;

            //If empty page, chage page
            if (gridView.Rows.Count == 1)
            {
                gridView.PageIndex = gridView.PageIndex - 1;
            }

            try
            {
                EventsAccessLayer.DeleteClientEvent(eventId, out bool isDeleted);

                if (isDeleted)
                {
                    BindGvUpcomingEvents();
                }
            }
            finally
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Eventandler for click to redirect AddParticipants.aspx
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnMoveToParticipants_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            GridView gridView = button.NamingContainer.NamingContainer as GridView;
            GridViewRow row = button.NamingContainer as GridViewRow;

            string eventId = gridView.DataKeys[row.RowIndex].Value.ToString();

            // string eventId = HttpUtility.UrlEncode(
            // Encription.EncryptedString(gridView.DataKeys[row.RowIndex].Value.ToString().Trim()));
            Response.Redirect("AddParticipants.aspx?ID=" + eventId);
        }

        /// <summary>
        ///Eventandler for click to redirect AddUserEvent.aspx
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnMoveToAddUserEvent_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddUserEvent.aspx");
        }
    }
}